package com.nomuffinlabs.gamereleases.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by robin on 28.11.2015.
 */
public class Game implements Serializable, Comparable<Game> {


    public static final String KEY_ID = "game_id";
    public static final String KEY_DATE_ID = "date_id";
    public static final String KEY_DATE_CATEGORY = "date_category";
    public static final String KEY_DATE = "date";
    public static final String KEY_PLATFORMS = "platforms";
    public static final String KEY_COVER_ID = "cover_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_SUMMARY = "summary";
    public static final String KEY_POPULARITY = "popularity";
    public static final String KEY_COMPANIES = "involved_companies_names";
    public static final String KEY_GAME_CATEGORIES = "game_categories_names";
    public static final String KEY_SCREENSHOTS = "screenshot_ids";
    public static final String KEY_SLUG = "slug";
    public static final String KEY_VIDEOS = "videos";
    public static final String KEY_WEBSITES = "websites_urls";
    public static final String KEY_URL = "url";
    public static final String KEY_REGION = "region";
    public static final String KEY_ARTWORKS_IDS = "artworks_ids";

    public int date_id;
    public int date_category;
    public long date;
    public ArrayList<String> platform_names;
    public int id;
    public String cover_id;
    public String name;
    public String summary;
    public float popularity;
    public ArrayList<String> company_names;
    public ArrayList<String> game_category_names;
    public ArrayList<String> screenshot_ids;
    public String slug;
    public ArrayList<Video> videos; //TODO serialize?
    public ArrayList<String> website_urls;
    public String url;
    public int region;
    public ArrayList<String> artworks_ids;


    public Game(int date_id, int date_category, long date, ArrayList<String> platform_names, int id,
                String cover_id, String name, String summary, float popularity, ArrayList<String> company_names,
                ArrayList<String> game_category_names, ArrayList<String> screenshot_ids, String slug, ArrayList<Video> videos,
                ArrayList<String> website_urls ,String url, int region, ArrayList<String> artworks_ids) {

        this.date_id = date_id;
        this.date_category = date_category;
        this.date = date;
        this.platform_names = platform_names;
        this.id = id;
        this.cover_id = cover_id;
        this.name = name;
        this.summary = summary;
        this.popularity = popularity;
        this.company_names = company_names;
        this.game_category_names = game_category_names;
        this.screenshot_ids = screenshot_ids;
        this.slug = slug;
        this.videos = videos;
        this.website_urls = website_urls;
        this.url = url;
        this.region = region;
        this.artworks_ids = artworks_ids;

    }

    public String platformsToString() {
        String allPlats = "";
        for (String platform :
                platform_names) {
            allPlats += platform + ",";
        }
        return allPlats.substring(0, allPlats.length() - 1);
    }

    @Override
    public int compareTo(Game game) {
        return new Date(date).compareTo(new Date(game.date));
    }
}
