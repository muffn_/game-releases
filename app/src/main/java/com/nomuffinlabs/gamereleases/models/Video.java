package com.nomuffinlabs.gamereleases.models;

import java.io.Serializable;

public class Video implements Serializable {

    public String name;
    public String id;

    public Video(String name, String id) {
        this.name = name;
        this.id = id;
    }

}
