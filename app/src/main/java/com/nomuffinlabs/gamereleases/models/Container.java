package com.nomuffinlabs.gamereleases.models;

import androidx.annotation.NonNull;

import java.io.Serializable;

/**
 * Created by robin on 30.10.2016.
 */

public class Container implements Serializable, Comparable<Container> {

    int id = 0;
    String name = null;
    String url = null;

    public Container(int id, String name, String url) {
        this.id = id;
        this.name = name;
        this.url = url;
    }

    @Override
    public int compareTo(@NonNull Container o) {
        return name.compareTo(o.name);
    }


}
