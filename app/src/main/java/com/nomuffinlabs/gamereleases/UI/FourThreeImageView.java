package com.nomuffinlabs.gamereleases.UI;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by robin on 25.09.2016.
 */

public class FourThreeImageView extends androidx.appcompat.widget.AppCompatImageView {

    public FourThreeImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthSpec, int heightSpec) {
        int fourThreeHeight = MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(widthSpec) * 3 / 4,
                MeasureSpec.EXACTLY);
        super.onMeasure(widthSpec, fourThreeHeight);
    }
}
