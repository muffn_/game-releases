package com.nomuffinlabs.gamereleases.UI;

import android.content.Intent;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomuffinlabs.gamereleases.R;
import com.nomuffinlabs.gamereleases.activities.GameViewPagerActivity;
import com.nomuffinlabs.gamereleases.models.Game;
import com.nomuffinlabs.gamereleases.utils.MyUtils;

import java.util.Collections;

import static com.nomuffinlabs.gamereleases.handlers.DataHandler.favoritesList;
import static com.nomuffinlabs.gamereleases.utils.MyUtils.debug;

public class GameViewHolder extends RecyclerView.ViewHolder {

    public Game game;

    MyAdapter myAdapter;
    ImageView cover;
    RelativeLayout cover_layout;
    RelativeLayout fav;
    ImageButton fav_on, reminder, openinbrowser, google;
    TextView tv_title, tv_date;
    ProgressBar progBar;
    LinearLayout bottom_layout;
    CardView container;

    GameViewHolder(final View v, final MyAdapter myAdapter) {
        super(v);

        this.myAdapter = myAdapter;

        tv_title = v.findViewById(R.id.tv_title);
        tv_date = v.findViewById(R.id.tv_date);
        fav = v.findViewById(R.id.fav);
        fav_on = v.findViewById(R.id.fav_on);
        reminder = v.findViewById(R.id.reminder);
        openinbrowser = v.findViewById(R.id.openinbrowser);
        google = v.findViewById(R.id.google);
        cover = v.findViewById(R.id.image);
        cover_layout = v.findViewById(R.id.cover_layout);
        bottom_layout = v.findViewById(R.id.bottom_layout);
        progBar = v.findViewById(R.id.progressbar);
        container = v.findViewById(R.id.container);

        container.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent = new Intent(myAdapter.context, GameViewPagerActivity.class);
                        intent.putExtra("list", myAdapter.list);
                        intent.putExtra("pos", getAdapterPosition());
                        myAdapter.context.startActivity(intent);
                    }
                }
        );


        //ugly clicklistener
        fav.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (fav_on.getVisibility() == View.VISIBLE) {
                            removeFromFavorites();
                        } else {
                            addToFavorites();
                        }

                        //TODO refresh other views?
                    }
                }
        );

    }

    public void addToFavorites() {

        if (!MyUtils.listContains(favoritesList, game.id)) {

            favoritesList.add(game);
            Collections.sort(favoritesList);

            int pos = MyUtils.getPosById(favoritesList, game.id);
            if (pos >= 0)
                myAdapter.notifyItemInserted(pos);

            fav_on.setVisibility(View.VISIBLE);
            MyUtils.showFirstFavoriteTimeReminderDialog(myAdapter.context);
        }

    }

    public void removeFromFavorites() {
        int pos = MyUtils.getPosById(favoritesList, game.id);

        favoritesList.remove(pos);
        myAdapter.notifyItemRemoved(pos);

        fav_on.setVisibility(View.INVISIBLE);
    }
}