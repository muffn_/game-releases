package com.nomuffinlabs.gamereleases.UI;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;

import com.nomuffinlabs.gamereleases.handlers.DataHandler;
import com.nomuffinlabs.gamereleases.utils.MyUtils;
import com.nomuffinlabs.gamereleases.R;
import com.nomuffinlabs.gamereleases.utils.SharedPrefHelper;

import java.util.ArrayList;
import java.util.Arrays;

import static com.nomuffinlabs.gamereleases.activities.MainActivity.debug;
import static com.nomuffinlabs.gamereleases.handlers.DataHandler.allPlatforms;
import static com.nomuffinlabs.gamereleases.handlers.DataHandler.getPreferredPlatforms;
import static com.nomuffinlabs.gamereleases.utils.SharedPrefHelper.SHAREDPREF_WANTED_PLATFORMS;

/**
 * Created by robin on 26-Sep-17.
 */

public class PlatformsAdapter extends ArrayAdapter<String> {

    private ArrayList<String> list;
    public ArrayList<String> currentPreferredPlatforms;

    public PlatformsAdapter(Context context, int textViewResourceId,
                            ArrayList<String> list) {

        super(context, textViewResourceId, list);

        this.list = list;

        //getting current platforms
        currentPreferredPlatforms = DataHandler.getPreferredPlatforms(getContext());
    }


    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        @SuppressLint("ViewHolder")
        View rowView = inflater.inflate(R.layout.platform_checkbox_item, parent, false);
        LinearLayout layout = rowView.findViewById(R.id.layout);
        final AppCompatCheckBox checkBox = rowView.findViewById(R.id.checkBox1);


        final String thisPlat = list.get(position);



        checkBox.setText(thisPlat);
        //is in current set platforms? than check that shit!
        if (currentPreferredPlatforms.contains(thisPlat)) {
            checkBox.setChecked(true);
        } else {
            checkBox.setChecked(false);
        }


        //changing values
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //is set? now disable it and remove it from list
                if (currentPreferredPlatforms.contains(thisPlat)) {

                    if (currentPreferredPlatforms.size() == 1) {
                        MyUtils.mToast("You must select at least one platform", true, getContext());
                    } else {
                        checkBox.setChecked(false);
                        currentPreferredPlatforms.remove(thisPlat);
                    }

                } else { //is not set? then activate and add it to list
                    checkBox.setChecked(true);
                    currentPreferredPlatforms.add(thisPlat);

                }

                String[] array = currentPreferredPlatforms.toArray(new String[currentPreferredPlatforms.size()]);
                SharedPrefHelper.setStringSet(SHAREDPREF_WANTED_PLATFORMS, array, getContext());

                debug("array: " + String.join(", ", getPreferredPlatforms(getContext())));
            }
        });


        return rowView;

    }
}