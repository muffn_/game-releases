package com.nomuffinlabs.gamereleases.UI;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.ProgressBar;

import com.nomuffinlabs.gamereleases.handlers.ImageHandler;
import com.nomuffinlabs.gamereleases.utils.OnLoadMoreListener;
import com.nomuffinlabs.gamereleases.models.Game;
import com.nomuffinlabs.gamereleases.utils.MyUtils;
import com.nomuffinlabs.gamereleases.R;
import com.nomuffinlabs.gamereleases.utils.SharedPrefHelper;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.nomuffinlabs.gamereleases.handlers.DataHandler.favoritesList;


public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<String> loaded = new ArrayList<>();

    public Context context;
    public ArrayList<Game> list;
    private OnLoadMoreListener mOnLoadMoreListener;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private int visibleThreshold = 10;
    private int lastVisibleItem;
    public boolean isLoading;

    public MyAdapter(Context context, RecyclerView mRecyclerView, ArrayList<Game> listt) {

        //this.instance = ins;
        this.list = listt;
        this.context = context;

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (!isLoading && list.size() <= (lastVisibleItem + visibleThreshold)) {

                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public int getItemViewType(int position) {
        return list.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }
    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }






    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {

        if (viewType == VIEW_TYPE_ITEM) {

            View v;
            if ( SharedPrefHelper.getBool("grid_view", true, context)) {
                v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_grid, parent, false);

            } else {
                v = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_item_list, parent, false);

            }
            return new GameViewHolder(v, this);

        } else if (viewType == VIEW_TYPE_LOADING) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_loading, parent, false);
            return new LoadingViewHolder(v);

        }
        return null;
    }


    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;

        LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar1);
        }
    }





    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder hholder, int position) {
        lastVisibleItem = hholder.getAdapterPosition();


        if (hholder instanceof LoadingViewHolder) {

            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) hholder;
            loadingViewHolder.progressBar.setIndeterminate(true);

        } else if (hholder instanceof GameViewHolder) {

            final GameViewHolder holder = (GameViewHolder) hholder;

            final String cover_id;
            final String title;
            long millis;

            holder.game = list.get(position);

            cover_id = holder.game.cover_id;
            title = holder.game.name;
            millis = holder.game.date * 1000;

            holder.cover.setImageDrawable(null);

            //Title
            holder.tv_title.setText(title);

            //daysleft
            String text = SharedPrefHelper.getBool("show_weeks_left", false, context) ? MyUtils.getWeeksLeftString(millis) : MyUtils.getDaysLeftString(millis);
            holder.tv_date.setText(text);



            //Fav
            if (MyUtils.listContains(favoritesList, holder.game.id))
                holder.fav_on.setVisibility(View.VISIBLE);
            else
                holder.fav_on.setVisibility(View.INVISIBLE);



            //Cover
            if (cover_id.equals("null") || cover_id.isEmpty()
                    || SharedPrefHelper.getBool("hide_cover", false, context) ) {

                showCover(holder, false);

            } else {

                showCover(holder, true);
                loadCover(holder);
            }
        }
    }

    private void loadCover(final GameViewHolder holder) {

        String url = ImageHandler.getCoverUrl(holder.game.cover_id, context);

        holder.progBar.setVisibility(View.VISIBLE);
        Picasso.get()
                .load(url)
                .into(holder.cover, new Callback() {
                    @Override
                    public void onSuccess() {

                        holder.cover.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                            @Override
                            public void onLayoutChange(View view, int i, int i1, int i2, int i3, int i4, int i5, int i6, int i7) {

                                //hide loading bar
                                holder.progBar.setVisibility(View.GONE);

                                //check if animation was already done
                                if (loaded.contains(holder.game.name)) {
                                    return;
                                }
                                loaded.add(holder.game.name);

                                //animate if required version
                                if ( android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP ) {

                                    if (SharedPrefHelper.getBool(context.getString(R.string.cover_reveal_animation), true, context)) {
                                        revealCoverAnimation(holder);
                                    }

                                    if (SharedPrefHelper.getBool(context.getString(R.string.cover_bw_animation), true, context)) {
                                        blackWhiteCoverAnimation(holder);
                                    }
                                }
                            }
                        });
                    }

                    @Override
                    public void onError(Exception e) {
                        debug("error while loading cover for: "+holder.game.name);

                    }
                });
    }


    public void revealCoverAnimation(GameViewHolder holder) {
        int cx = (holder.cover.getLeft() + holder.cover.getRight()) / 2;
        int cy = (holder.cover.getTop() + holder.cover.getBottom()) / 2;

        int dx = Math.max(cx, holder.cover.getWidth() - cx);
        int dy = Math.max(cy, holder.cover.getHeight() - cy);
        float finalRadius = (float) Math.hypot(dx, dy);

        Animator animator =
                ViewAnimationUtils.createCircularReveal(holder.cover, cx, cy, 0, finalRadius);
        animator.setInterpolator(new FastOutSlowInInterpolator());
        animator.setDuration(800);
        animator.start();
    }

    public void blackWhiteCoverAnimation(final GameViewHolder holder) {
        final ColorMatrix matrix = new ColorMatrix();
        ValueAnimator animation = ValueAnimator.ofFloat(0f, 1f);
        animation.setDuration(2000);
        animation.setInterpolator(new LinearInterpolator());
        animation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                matrix.setSaturation(animation.getAnimatedFraction());
                ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
                holder.cover.setColorFilter(filter);
            }
        });
        animation.start();
    }


    public void showCover(GameViewHolder holder, boolean state) {
        if ( state )
            holder.cover_layout.setVisibility(View.VISIBLE);
        else
            holder.cover_layout.setVisibility(View.GONE);
    }


    public static void debug(String message) {
        Log.e("MyAdapter", message);
    }

}