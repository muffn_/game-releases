package com.nomuffinlabs.gamereleases.fragments;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.core.content.res.ResourcesCompat;
import androidx.appcompat.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nomuffinlabs.gamereleases.handlers.CalendarHandler;
import com.nomuffinlabs.gamereleases.handlers.ImageHandler;
import com.nomuffinlabs.gamereleases.models.Game;
import com.nomuffinlabs.gamereleases.models.Video;
import com.nomuffinlabs.gamereleases.utils.MyUtils;
import com.nomuffinlabs.gamereleases.R;
import com.nomuffinlabs.gamereleases.UI.FlowLayout;
import com.nomuffinlabs.gamereleases.activities.FilterActivity;
import com.nomuffinlabs.gamereleases.activities.ImageViewerPagerActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;

import static com.nomuffinlabs.gamereleases.handlers.DataHandler.favoritesList;

public class GameViewFragment extends Fragment {

    Game game;


    public int date_id;
    public int date_category;
    public long date;
    public ArrayList<String> platform_names;
    public int id;
    public String cover_id;
    public String name;
    public String summary;
    public float popularity;
    public ArrayList<String> company_names;
    public ArrayList<String> game_category_names;
    public ArrayList<String> screenshot_ids;
    public String slug;
    public ArrayList<Video> videos; //TODO serialize?
    public ArrayList<String> website_urls;
    public String url;
    public int region;
    public ArrayList<String> artworks_ids;


    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;

    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;

    @BindView(R.id.image)
    ImageView image;

    @BindView(R.id.title)
    TextView tv_title;

    @BindView(R.id.summary)
    TextView tv_summary;

    @BindView(R.id.cats_text)
    TextView tv_categories;

    @BindView(R.id.tv_screenshots)
    TextView tv_screenshots;

    @BindView(R.id.tv_videos)
    TextView tv_videos;

    @BindView(R.id.openinbrowser)
    ImageButton openinbrowser;
    @BindView(R.id.tv_openinbrowser)
    TextView tv_openinbrowser;

    @BindView(R.id.addtocalendar)
    ImageButton addtocalendar;

    @BindView(R.id.addtocalendar_layouut)
    LinearLayout addtocalendar_layouut;

    @BindView(R.id.google)
    ImageButton gsearch;
    @BindView(R.id.tv_google)
    TextView tv_google;

    @BindView(R.id.reminder)
    ImageButton reminder;
    @BindView(R.id.tv_remind)
    TextView tv_remind;

    @BindView(R.id.tv_websites)
    TextView tv_websites;

    @BindView(R.id.fav)
    FloatingActionButton fav;

    @BindView(R.id.linear)
    LinearLayout horizontal_screenshots;

    @BindView(R.id.linear_videos)
    LinearLayout horizontal_videos;

    @BindView(R.id.linear_websites)
    LinearLayout list_websites;

    @BindView(R.id.linear_genres)
    FlowLayout horizontal_categories;

    @BindDrawable(R.drawable.ic_favorite_border_black_24dp)
    Drawable fav_off_black;
    @BindDrawable(R.drawable.ic_favorite_black_24dp)
    Drawable fav_on_black;
    @BindDrawable(R.drawable.ic_favorite_border_white_24dp)
    Drawable fav_off_white;
    @BindDrawable(R.drawable.ic_favorite_white_24dp)
    Drawable fav_on_white;


    @BindView(R.id.platform)
            TextView platform;
    @BindView(R.id.daysleft)
        TextView daysleft;
    @BindView(R.id.date)
        TextView tv_date;

    Context context;


    public GameViewFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public ViewGroup rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = (ViewGroup) inflater.inflate(R.layout.fragment_gameview, container, false);
        ButterKnife.bind(this, rootView);
        context = getActivity();

        settingUp();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        settingFavButton();
    }


    private void settingUp() {
        initData();

        handleLayout();

        initListeners();

    }


    public void initData() {
        /*if (getIntent().getAction() == Intent.ACTION_VIEW) {
            //TODO edit below and fix this shit
            //Come from url
            MyUtils.mToast("Feature not available currently", true, this);
            getActivity().finish();
        } else {

            game = (Game) getIntent().getSerializableExtra("Game");

            if ( game == null) {
                MyUtils.mToast("Something went wrong!", true, this);
                getActivity().finish();
            }

            title = game.title;
            id = game.id;
            summary = game.summary;
            screenshots = game.screenshots;
            themes = game.themes;
            genres = game.genres;
            game_modes = game.game_modes;

            releaseDates = game.releaseDates;
            cover_id = game.cover_id;

        }*/

        game = (Game) getArguments().getSerializable("game");

        if (game == null) {
            MyUtils.mToast("Something went wrong!", true, getActivity());
            getActivity().finish();
        }

        name = game.name;
        id = game.id;
        summary = game.summary;
        screenshot_ids = game.screenshot_ids;
        game_category_names = game.game_category_names;
        website_urls = game.website_urls;
        videos = game.videos;
        date = game.date;
        cover_id = game.cover_id;
    }


    public void handleLayout() {

        settingToolbarCover(image);

        Typeface typeface = Typeface.create(Typeface.MONOSPACE, Typeface.NORMAL);

        collapsingToolbarLayout.setExpandedTitleTypeface(typeface);
        collapsingToolbarLayout.setCollapsedTitleTypeface(typeface);
        collapsingToolbarLayout.setTitle(name);
        collapsingToolbarLayout.setExpandedTitleTypeface(typeface);
        collapsingToolbarLayout.setCollapsedTitleTypeface(typeface);

        tv_title.setText(name);

        Toolbar toolbar = rootView.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.back_material);
        //setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        if (summary == null || (summary.isEmpty() && summary.equals("")))
            summary = "No description available";

        tv_summary.setText(summary);

        platform.setText(game.platformsToString());
        daysleft.setText(MyUtils.getDaysLeftString(date * 1000));
        tv_date.setText(MyUtils.getFormattedDate(date * 1000, MyUtils.DATE_FORMAT_ddMMyyyy));

        settingFavButton();
        settingWebsites();
        settingVideos();
        settingCategories();
        settingScreenshots();

    }


    private void initListeners() {
        fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (MyUtils.listContains(favoritesList, game.id)) {

                    fav.setImageDrawable(fav_off_white);

                    int pos = MyUtils.getPosById(favoritesList, game.id);
                    favoritesList.remove(pos);

                } else {

                    fav.setImageDrawable(fav_on_white);
                    favoritesList.add(game);
                    Collections.sort(favoritesList);
                    MyUtils.showFirstFavoriteTimeReminderDialog(context);
                }
            }
        });

        gsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyUtils.openUrlInCustomTab(context, "https://www.google.de/search?q=" + name, Color.DKGRAY);
            }
        });
        reminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
                builder.setTitle("Add as event in your calendar?")
                        .setMessage("Do you want to add an event for this game in your calendar?")
                        .setCancelable(true)
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                CalendarHandler.getResultsFromApi();
                            }
                        }).create().show();
            }
        });

        openinbrowser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //currently open in browser
                MyUtils.openUrlInCustomTab(context, game.url, Color.DKGRAY);
            }
        });
        addtocalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //TODO ???????????????????????????????????????????????????????????
                /*final ArrayList<ReleaseDate> temp_dates = game.releaseDates;
                for (int i = 0; i < temp_dates.size(); i++) {
                    long date = temp_dates.get(i).date;
                    if (MyUtils.getFormattedDate(date, MyUtils.DATE_FORMAT_ddMMyyyy).length() == 4) //2018
                        temp_dates.remove(i);
                    else if (System.currentTimeMillis() > date)
                        temp_dates.remove(i);
                }

                if (temp_dates.size() == 1) {
                    Intent intent = new Intent(Intent.ACTION_EDIT);
                    intent.setType("vnd.android.cursor.item/event");
                    intent.putExtra(CalendarContract.Events.TITLE, title);
                    intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,
                            temp_dates.get(0).date);
                    intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,
                            temp_dates.get(0).date);
                    intent.putExtra(CalendarContract.Events.ALL_DAY, true);// periodicity
                    intent.putExtra(CalendarContract.Events.DESCRIPTION, "For " + MyUtils.getPlatformName(temp_dates.get(0).platform) + "\n" + game.summary);
                    startActivity(intent);
                } else {
                    AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
                    builderSingle.setTitle("Choose one release date for \"" + game.title + "\"");

                    final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.select_dialog_singlechoice);
                    for (ReleaseDate date : temp_dates) {
                        String platform_name = MyUtils.getPlatformName(date.platform);
                        String releasedate = MyUtils.getFormattedDate(date.date, MyUtils.DATE_FORMAT_MMMdyyyy);
                        arrayAdapter.add(platform_name + ": " + releasedate + " - " + MyUtils.getDaysLeft(date.date) + " days left");
                    }

                    builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            debug("releasedate: " + temp_dates.get(which));

                            Intent intent = new Intent(Intent.ACTION_EDIT);
                            intent.setType("vnd.android.cursor.item/event");
                            intent.putExtra(CalendarContract.Events.TITLE, title);
                            intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,
                                    temp_dates.get(which).date);
                            intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,
                                    temp_dates.get(which).date);
                            intent.putExtra(CalendarContract.Events.ALL_DAY, true);// periodicity
                            intent.putExtra(CalendarContract.Events.DESCRIPTION, "For " + MyUtils.getPlatformName(game.releaseDates.get(which).platform) + "\n" + game.summary);
                            startActivity(intent);
                        }
                    });
                    builderSingle.show();
                }*/
            }
        });
    }


    public void settingWebsites() {

        for (int i = 0; i < this.website_urls.size(); i++) {

            String site = this.website_urls.get(i);

            site = site.replace("https://", "");
            site = site.replace("http://", "");

            if (!site.contains("www.")) {
                site = "www." + site;
            }

            if (site.endsWith("/")) {
                site = site.substring(0, site.length() - 1);
            }

            TextView tv = new TextView(context);
            tv.setPadding(0, 10, 5, 10);
            tv.setTextColor(MyUtils.primarytextcolor);
            tv.setText(site);
            tv.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
            tv.setTextSize(14);

            /*LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            llp.setMargins(0, 0, 0, 0); // llp.setMargins(left, top, right, bottom);
            tv.setLayoutParams(llp);
*/
            TypedValue outValue = new TypedValue();
            getContext().getTheme().resolveAttribute(android.R.attr.selectableItemBackground, outValue, true);
            tv.setBackgroundResource(outValue.resourceId);

            final int finalI = i;
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MyUtils.openUrlInCustomTab(context, GameViewFragment.this.website_urls.get(finalI), MyUtils.colorPrimary);
                }
            });

            list_websites.addView(tv);
        }

        if (list_websites.getChildCount() == 0) {
            tv_websites.setVisibility(View.GONE);
        }
    }


    //videos: [{"name":"Trailer","video_id":"HO-qKPsLuss"}, {"name":"Gameplay video","video_id":"oDoZ1gMZRro"}, {"name":"Gameplay video","video_id":"ebWx6Lf6-JQ"}]
    public void settingVideos() {
        if (videos.size() == 0) {
            tv_videos.setVisibility(View.GONE);
            horizontal_videos.setVisibility(View.GONE);
        }

        for (int i = 0; i < videos.size(); i++) {

            Video video = videos.get(i);
            addItemToHorizontalView_Video(video.name, video.id, i);
        }
    }

    public void settingFavButton() {
        if (MyUtils.listContains(favoritesList, game.id)) {
            fav.setImageDrawable(fav_on_white);
        } else {
            fav.setImageDrawable(fav_off_white);
        }
    }

    public void settingToolbarCover(final ImageView view) {
        if (cover_id.isEmpty() || cover_id.equals("") || cover_id == null || cover_id.equals("null")) {
            //appBarLayout.setExpanded(false, true);
        } else {
            Picasso.get()
                    .load(ImageHandler.getCoverUrl(cover_id, context))
                    .into(view, new Callback() {
                        @Override
                        public void onSuccess() {
                            setToolbarImageClickListener(view);
                        }

                        @Override
                        public void onError(Exception e) {
                            //appBarLayout.setExpanded(false, true);
                        }
                    });
        }
    }

    public void setToolbarImageClickListener(final ImageView toolbar_image) {
        toolbar_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (toolbar_image.getDrawable() != null) {
                    ArrayList<String> image = new ArrayList<>();
                    image.add(cover_id);

                    Intent intent = new Intent(context, ImageViewerPagerActivity.class);
                    intent.putExtra("image_ids", image);
                    intent.putExtra("pos", 0);

                    //values for covers
                    ArrayList<String> quality_names = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.covers_qualities_names)));
                    ArrayList<String> quality_values = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.covers_qualities_values)));

                    intent.putStringArrayListExtra("quality_names", quality_names);
                    intent.putStringArrayListExtra("quality_values", quality_values);

                    context.startActivity(intent);

                }
            }
        });
    }

    String CATS = "cats";

    public void settingCategories() {
        if (game_category_names.size() > 0) {

            for (int i = 0; i < game_category_names.size(); i++) {

                final String name = game_category_names.get(i);

                addItemToHorizontalView(horizontal_categories, 0, name, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), FilterActivity.class);
                        intent.putExtra("filter_value", name);
                        startActivity(intent);
                    }
                });
            }
        } else {
            tv_categories.setVisibility(View.GONE);
            horizontal_categories.setVisibility(View.GONE);
        }
    }


    public void settingScreenshots() {
        //image_ids

        if (screenshot_ids.size() > 0 && !screenshot_ids.get(0).isEmpty() ) {
            for (int i = 0; i < screenshot_ids.size(); i++) {
                debug("screenshot: "+screenshot_ids.get(i));

                String id = screenshot_ids.get(i);
                String url = ImageHandler.getScreenshotUrl(id, context);
                addItemToHorizontalView_Screenshot(url, i, 1);
            }
        } else {
            rootView.findViewById(R.id.horizontal_scroll).setVisibility(View.GONE);
            tv_screenshots.setVisibility(View.GONE);
        }
    }


    public void addItemToHorizontalView_Screenshot(final String url, final int i, int ii) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        final RelativeLayout layout = (RelativeLayout) inflater.inflate(R.layout.screenshot_layout, null, false);

        final ImageButton image = layout.findViewById(R.id.image);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        Picasso.get().load(url).into(image, new Callback() {
            @Override
            public void onSuccess() {
                if (!url.equals("-1")) {
                    image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(context, ImageViewerPagerActivity.class);
                            intent.putExtra("pos", i);
                            intent.putStringArrayListExtra("image_ids", screenshot_ids);

                            //values for screenshits lawl
                            ArrayList<String> quality_names = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.screenshots_qualities_names)));
                            ArrayList<String> quality_values = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.screenshots_qualities_values)));

                            intent.putStringArrayListExtra("quality_names", quality_names);
                            intent.putStringArrayListExtra("quality_values", quality_values);

                            getActivity().startActivity(intent);
                        }
                    });

                    LinearLayout.LayoutParams relativeParams = new LinearLayout.LayoutParams(
                            new LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.MATCH_PARENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT));


                    if (i == 0) {
                        relativeParams.setMargins(0, 0, 20, 0);
                    } else if (i == videos.size() - 1) {
                        relativeParams.setMargins(20, 0, 0, 0);
                    } else {
                        relativeParams.setMargins(20, 0, 20, 0);
                    }
                    layout.setLayoutParams(relativeParams);
                    layout.requestLayout();

                }
            }

            @Override
            public void onError(Exception e) {
                debug("screenshots: onError: "+url);
                layout.setBackground(null);
            }
        });
        horizontal_screenshots.addView(layout);
    }

    public void addItemToHorizontalView_Video(String name, final String video_id, final int i) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        final LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.video_layout, null, false);

        final ImageButton image = layout.findViewById(R.id.image);
        final TextView tv_name = layout.findViewById(R.id.name);
        tv_name.setText(name);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        final String url = "https://img.youtube.com/vi/" + video_id + "/0.jpg";
        Picasso.get().load(url).into(image, new Callback() {
            @Override
            public void onSuccess() {
                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + video_id));
                        Intent webIntent = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("https://www.youtube.com/watch?v=" + video_id));
                        try {
                            startActivity(appIntent);
                        } catch (ActivityNotFoundException ex) {
                            startActivity(webIntent);
                        }
                    }
                });

                LinearLayout.LayoutParams relativeParams = new LinearLayout.LayoutParams(
                        new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT));


                if (i == 0) {
                    relativeParams.setMargins(0, 0, 20, 0);
                } else if (i == videos.size() - 1) {
                    relativeParams.setMargins(20, 0, 0, 0);
                } else {
                    relativeParams.setMargins(20, 0, 20, 0);
                }
                layout.setLayoutParams(relativeParams);
                layout.requestLayout();

            }

            @Override
            public void onError(Exception e) {
                debug("videos: onError: "+url);
                layout.setBackground(null);
            }
        });
        horizontal_videos.addView(layout);
    }


    public void addItemToHorizontalView(FlowLayout v, int id, final String buttonText, View.OnClickListener onClickListener) {
        Button btn = new Button(context);
        btn.setId(id);
        btn.setText(buttonText);
        btn.setOnClickListener(onClickListener);

        int colorPrimary = ResourcesCompat.getColor(getResources(), R.color.colorPrimary, null);
        int textcolor = ResourcesCompat.getColor(getResources(), R.color.primaryTextColor, null);
        btn.setTextColor(textcolor);
        btn.getBackground().setColorFilter(textcolor, PorterDuff.Mode.MULTIPLY);
        btn.setTextColor(colorPrimary);
        v.addView(btn);
    }


    public void debug(String message) {
        Log.e("Fragment_GameView: ", message);
    }

}