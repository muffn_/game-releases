package com.nomuffinlabs.gamereleases.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import android.view.View;
import android.widget.ListView;

import com.nomuffinlabs.gamereleases.R;
import com.nomuffinlabs.gamereleases.UI.PlatformsAdapter;
import com.nomuffinlabs.gamereleases.activities.MainActivity;
import com.nomuffinlabs.gamereleases.utils.MyUtils;

import java.util.Objects;

import static com.nomuffinlabs.gamereleases.activities.MainActivity.debug;
import static com.nomuffinlabs.gamereleases.handlers.DataHandler.filteredMainList;
import static com.nomuffinlabs.gamereleases.handlers.DataHandler.allPlatforms;
import static com.nomuffinlabs.gamereleases.handlers.DataHandler.popularList;
import static com.nomuffinlabs.gamereleases.handlers.DataHandler.releasedList;

/**
 * Created by robin on 26-Sep-17.
 */

public class BottomSheetDialogPlatformsFragment extends BottomSheetDialogFragment {

    Context context;

    public BottomSheetDialogPlatformsFragment(Context context) {
        this.context = context;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.fragment_platforms_bottomsheet, null);
        dialog.setContentView(contentView);

        ListView listView = contentView.findViewById(R.id.platforms);
        PlatformsAdapter adapter = new PlatformsAdapter(getContext(), R.layout.platform_checkbox_item, allPlatforms);

        listView.setAdapter(adapter);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        debug("onDetach");

        filteredMainList.clear();
        popularList.clear();
        releasedList.clear();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                MyUtils.updateLists(context);
                ((MainActivity) context).refreshFragmentsAdapters();
            }
        }, 1000);
    }
}