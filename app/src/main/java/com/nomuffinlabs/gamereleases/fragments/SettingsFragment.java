package com.nomuffinlabs.gamereleases.fragments;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.preference.Preference;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListAdapter;
import com.afollestad.materialdialogs.simplelist.MaterialSimpleListItem;
import com.nomuffinlabs.gamereleases.BuildConfig;
import com.nomuffinlabs.gamereleases.utils.Changelog;
import com.nomuffinlabs.gamereleases.utils.GoogleAPI;
import com.nomuffinlabs.gamereleases.utils.MyUtils;
import com.nomuffinlabs.gamereleases.R;
import com.nomuffinlabs.gamereleases.activities.AboutActivity;
import com.nomuffinlabs.gamereleases.activities.MainActivity;


/*
public class SettingsFragment extends XpPreferenceFragment {

    SettingsActivity settingsActivity;
    public Context context;
    GoogleAPI api;

    public static SettingsFragment newInstance(String rootKey) {
        Bundle args = new Bundle();
        args.putString(SettingsFragment.ARG_PREFERENCE_ROOT, rootKey);
        SettingsFragment fragment = new SettingsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String[] getCustomDefaultPackages() {
        return new String[]{BuildConfig.APPLICATION_ID};
    }


    @Override
    public void onCreatePreferences2(final Bundle savedInstanceState, final String rootKey) {
        // Add 'general' preferences.
        addPreferencesFromResource(R.xml.pref_general);

        final Preference sync_pref = findPreference("sync");

        final Preference about_pref = findPreference("about");
        final Preference rate_pref = findPreference("rate");
        final Preference report_pref = findPreference("report");
        final Preference changelog_pref = findPreference("changelog");
        final Preference licenses_pref = findPreference("licenses");
        final Preference theme_pref = findPreference("theme_dark");
        final Preference hide_wno_cover_pref = findPreference("hide_wno_cover");
        final Preference accent_color_pref = findPreference("accent_color_key");
        final Preference grid_view_pref = findPreference("grid_view");
        final Preference hide_cover_pref = findPreference("hide_cover");

        //final Preference hide_released_pref = findPreference("hide_released");

        final Preference cover_pref = findPreference(getResources().getString(R.string.cover_quality_key));
        final Preference screenhot_pref = findPreference(getResources().getString(R.string.screenshot_quality_key));

        settingsActivity = ((SettingsActivity) getActivity());

        context = getContext();
        api = settingsActivity.api;

        sync_pref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (api.api.isConnecting() || api.api.isConnected()) {
                    api.api.disconnect();
                }
                Log.e("debug", "connecting...");
                api.apiConnect();

                return true;
            }
        });


        about_pref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent intent = new Intent(context, AboutActivity.class);
                startActivity(intent);
                return true;
            }
        });

        rate_pref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                new MaterialDialog.Builder(context)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
                                Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
                                try {
                                    startActivity(myAppLinkToMarket);
                                } catch (ActivityNotFoundException e) {
                                    Toast.makeText(context, " unable to find play store app", Toast.LENGTH_LONG).show();
                                }
                            }
                        })
                        .title("Would you like to rate this app?")
                        .backgroundColor(MyUtils.colorPrimary)
                        .contentColor(MyUtils.primarytextcolor)
                        .positiveText("Yea")
                        .negativeText("Nope")
                        .show();
                return true;
            }
        });

        report_pref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                new MaterialDialog.Builder(context)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                Intent i = new Intent(Intent.ACTION_SEND);
                                i.setType("message/rfc822");
                                i.putExtra(Intent.EXTRA_EMAIL, new String[]{"robin.rs80@gmail.com"});
                                i.putExtra(Intent.EXTRA_SUBJECT, "[REQUEST] ");
                                try {
                                    startActivity(Intent.createChooser(i, "Send mail..."));
                                } catch (android.content.ActivityNotFoundException ex) {
                                    Toast.makeText(context, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                Intent i = new Intent(Intent.ACTION_SEND);
                                i.setType("message/rfc822");
                                i.putExtra(Intent.EXTRA_EMAIL, new String[]{"robin.rs80@gmail.com"});
                                i.putExtra(Intent.EXTRA_SUBJECT, "[BUG] ");
                                try {
                                    startActivity(Intent.createChooser(i, "Send mail..."));
                                } catch (android.content.ActivityNotFoundException ex) {
                                    Toast.makeText(context, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .title("Would you like to send a report or feature request?")
                        .backgroundColor(MyUtils.colorPrimary)
                        .contentColor(MyUtils.primarytextcolor)
                        .positiveText("Feature Request")
                        .negativeText("Bug Report")
                        .neutralText("Nope")
                        .show();
                return true;
            }
        });

        /*hide_released_pref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override TODO
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                MainActivity.hideReleasedSettingsChanged = true;
                return true;
            }
        });*

        cover_pref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                MainActivity.qualitySettingsChanged = true;
                return true;
            }
        });
        screenhot_pref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                MainActivity.qualitySettingsChanged = true;
                return true;
            }
        });

        licenses_pref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                new MaterialDialog.Builder(context)
                        .title("Licenses")
                        .content(licenses)
                        .backgroundColor(MyUtils.colorPrimary)
                        .contentColor(MyUtils.primarytextcolor)
                        .positiveText("k")
                        .show();
                return true;
            }
        });
        theme_pref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                MainActivity.uiSettingsChanged = true;
                return true;
            }
        });
        accent_color_pref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                MainActivity.uiSettingsChanged = true;
                return true;
            }
        });
        grid_view_pref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                MainActivity.uiSettingsChanged = true;
                return true;
            }
        });
        hide_wno_cover_pref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                MainActivity.hideNoCoverGamesSettingschanged = true;
                return true;
            }
        });
        hide_cover_pref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                MainActivity.uiSettingsChanged = true;
                return true;
            }
        });
        changelog_pref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Changelog.showChangelog(getActivity());
                return true;
            }
        });

        getPreferenceScreen().setTitle("Settings");

        // Setup root preference.
        // Use with ReplaceFragment strategy.
        PreferenceScreenNavigationStrategy.ReplaceFragment.onCreatePreferences(this, rootKey);
    }

    public void showBackupRestoreDialog(final Context context) {

        final MaterialSimpleListAdapter adapter = new MaterialSimpleListAdapter(new MaterialSimpleListAdapter.Callback() {
            @Override
            public void onMaterialListItemSelected(MaterialDialog dialog, int index, MaterialSimpleListItem item) {
                if (index == 0) {

                    new MaterialDialog.Builder(context)
                            .title("Backup your favorites?")
                            .backgroundColor(MyUtils.colorPrimary)
                            .contentColor(MyUtils.primarytextcolor)
                            .positiveText("Yes")
                            .negativeText("Nope.")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    api.doDriveBackup();
                                    settingsActivity.showProgress();
                                }
                            })
                            .show();

                } else if (index == 1) {

                    new MaterialDialog.Builder(context)
                            .title("Restore your latest favorites?")
                            .backgroundColor(MyUtils.colorPrimary)
                            .contentColor(MyUtils.primarytextcolor)
                            .positiveText("Yes")
                            .negativeText("Nope.")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    api.restoreDriveBackup();
                                    settingsActivity.showProgress();
                                }
                            })
                            .show();

                }
                dialog.dismiss();
            }
        });

        int backup_icon = MyUtils.hasDarkTheme(context) ? R.drawable.ic_backup_white_24dp : R.drawable.ic_backup_black_24dp;
        int restore_icon = MyUtils.hasDarkTheme(context) ? R.drawable.ic_restore_white_24dp : R.drawable.ic_restore_black_24dp;

        adapter.add(new MaterialSimpleListItem.Builder(context)
                .content("Backup")
                .icon(backup_icon)
                .backgroundColor(MyUtils.colorPrimary)
                .build());
        adapter.add(new MaterialSimpleListItem.Builder(context)
                .content("Restore")
                .icon(restore_icon)
                .backgroundColor(MyUtils.colorPrimary)
                .build());

        new MaterialDialog.Builder(context)
                .title("Backup or restore your favorites")
                .adapter(adapter, null)
                .backgroundColor(MyUtils.colorPrimary)
                .contentColor(MyUtils.primarytextcolor)
                .show();

    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().setTitle(getPreferenceScreen().getTitle());
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final RecyclerView listView = getListView();

        // We're using alternative divider.
        setDivider(null);

        // We don't want this. The children are still focusable.
        listView.setFocusable(false);
    }


    Spanned licenses = Html.fromHtml(
            "<b>Android-Material-Design-Colors</b><br>" +
                    "Copyright 2014 wada811<br>" +
                    "<br>" +
                    "Licensed under the Apache License, Version 2.0 (the \"License\");<br>" +
                    "you may not use this file except in compliance with the License.<br>" +
                    "You may obtain a copy of the License at<br>" +
                    "<br>" +
                    "http://www.apache.org/licenses/LICENSE-2.0<br>" +
                    "<br>" +
                    "Unless required by applicable law or agreed to in writing, software<br>" +
                    "distributed under the License is distributed on an \"AS IS\" BASIS,<br>" +
                    "WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.<br>" +
                    "See the License for the specific language governing permissions and<br>" +
                    "limitations under the License.<br>" +
                    "<br>" +
                    "<br>" +
                    "<b>Butter Knife</b><br>" +
                    "Copyright 2013 Jake Wharton<br>" +
                    "<br>" +
                    "Licensed under the Apache License, Version 2.0 (the \"License\");<br>" +
                    "you may not use this file except in compliance with the License.<br>" +
                    "You may obtain a copy of the License at<br>" +
                    "<br>" +
                    "   http://www.apache.org/licenses/LICENSE-2.0<br>" +
                    "<br>" +
                    "Unless required by applicable law or agreed to in writing, software<br>" +
                    "distributed under the License is distributed on an \"AS IS\" BASIS,<br>" +
                    "WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.<br>" +
                    "See the License for the specific language governing permissions and<br>" +
                    "limitations under the License.<br>" +
                    "<br>" +
                    "<br>" +
                    "<b>CircularReveal</b><br>" +
                    "The MIT License (MIT)<br>" +
                    "<br>" +
                    "Copyright (c) 2016 Abdullaev Ozodrukh<br>" +
                    "<br>" +
                    "Permission is hereby granted, free of charge, to any person obtaining a copy<br>" +
                    "of this software and associated documentation files (the \"Software\"), to deal<br>" +
                    "in the Software without restriction, including without limitation the rights<br>" +
                    "to use, copy, modify, merge, publish, distribute, sublicense, and/or sell<br>" +
                    "copies of the Software, and to permit persons to whom the Software is<br>" +
                    "furnished to do so, subject to the following conditions:<br>" +
                    "<br>" +
                    "The above copyright notice and this permission notice shall be included in<br>" +
                    "all copies or substantial portions of the Software.<br>" +
                    "<br>" +
                    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR<br>" +
                    "IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,<br>" +
                    "FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE<br>" +
                    "AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER<br>" +
                    "LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,<br>" +
                    "OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN<br>" +
                    "THE SOFTWARE.<br>" +
                    "<br>" +
                    "<br>" +
                    "<b>EasyPermissions</b><br>" +
                    "Licensed under the Apache License, Version 2.0<br>" +
                    "<br>" +
                    "<br>" +
                    "<b>FloatingActionButton</b><br>" +
                    "Copyright 2015 Dmytro Tarianyk<br>" +
                    "<br>" +
                    "Licensed under the Apache License, Version 2.0 (the \"License\");<br>" +
                    "you may not use this file except in compliance with the License.<br>" +
                    "You may obtain a copy of the License at<br>" +
                    "<br>" +
                    "   http://www.apache.org/licenses/LICENSE-2.0<br>" +
                    "<br>" +
                    "Unless required by applicable law or agreed to in writing, software<br>" +
                    "distributed under the License is distributed on an \"AS IS\" BASIS,<br>" +
                    "WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.<br>" +
                    "See the License for the specific language governing permissions and<br>" +
                    "limitations under the License.<br>" +
                    "<br>" +
                    "<br>" +
                    "<b>Material Design Icons</b><br>" +
                    "Licensed under the Apache License, Version 2.0<br>" +
                    "<br>" +
                    "<br>" +
                    "<b>Material Dialogs</b><br>" +
                    "The MIT License (MIT)<br>" +
                    "<br>" +
                    "Copyright (c) 2014-2016 Aidan Michael Follestad<br>" +
                    "<br>" +
                    "Permission is hereby granted, free of charge, to any person obtaining a copy<br>" +
                    "of this software and associated documentation files (the \"Software\"), to deal<br>" +
                    "in the Software without restriction, including without limitation the rights<br>" +
                    "to use, copy, modify, merge, publish, distribute, sublicense, and/or sell<br>" +
                    "copies of the Software, and to permit persons to whom the Software is<br>" +
                    "furnished to do so, subject to the following conditions:<br>" +
                    "<br>" +
                    "The above copyright notice and this permission notice shall be included in all<br>" +
                    "copies or substantial portions of the Software.<br>" +
                    "<br>" +
                    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR<br>" +
                    "IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,<br>" +
                    "FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE<br>" +
                    "AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER<br>" +
                    "LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,<br>" +
                    "OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE<br>" +
                    "SOFTWARE.<br>" +
                    "<br>" +
                    "<br>" +
                    "<b>Material Preference</b><br>" +
                    "Licensed under the Apache License, Version 2.0" +
                    "<br>" +
                    "<br>" +
                    "<b>MaterialSearchView</b><br>" +
                    "Copyright 2015 Miguel Catalan Bañuls<br>" +
                    "<br>" +
                    "Licensed under the Apache License, Version 2.0 (the \"License\");<br>" +
                    "you may not use this file except in compliance with the License.<br>" +
                    "You may obtain a copy of the License at<br>" +
                    "<br>" +
                    "\thttp://www.apache.org/licenses/LICENSE-2.0<br>" +
                    "<br>" +
                    "Unless required by applicable law or agreed to in writing, software<br>" +
                    "distributed under the License is distributed on an \"AS IS\" BASIS,<br>" +
                    "WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.<br>" +
                    "See the License for the specific language governing permissions and<br>" +
                    "limitations under the License.<br>" +
                    "<br>" +
                    "<br>" +
                    "<b>OkHttp</b><br>" +
                    "Licensed under the Apache License, Version 2.0 (the \"License\");<br>" +
                    "you may not use this file except in compliance with the License.<br>" +
                    "You may obtain a copy of the License at<br>" +
                    "<br>" +
                    "   http://www.apache.org/licenses/LICENSE-2.0<br>" +
                    "<br>" +
                    "Unless required by applicable law or agreed to in writing, software<br>" +
                    "distributed under the License is distributed on an \"AS IS\" BASIS,<br>" +
                    "WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.<br>" +
                    "See the License for the specific language governing permissions and<br>" +
                    "limitations under the License.<br>" +
                    "<br>" +
                    "<br>" +
                    "<b>PhotoView</b><br>" +
                    "Copyright 2017 Chris Banes<br>" +
                    "<br>" +
                    "Licensed under the Apache License, Version 2.0 (the \"License\");<br>" +
                    "you may not use this file except in compliance with the License.<br>" +
                    "You may obtain a copy of the License at<br>" +
                    "<br>" +
                    "   http://www.apache.org/licenses/LICENSE-2.0<br>" +
                    "<br>" +
                    "Unless required by applicable law or agreed to in writing, software<br>" +
                    "distributed under the License is distributed on an \"AS IS\" BASIS,<br>" +
                    "WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.<br>" +
                    "See the License for the specific language governing permissions and<br>" +
                    "limitations under the License.<br>" +
                    "<br>" +
                    "<br>" +
                    "<b>Picasso</b><br>" +
                    "Copyright 2013 Square, Inc.<br>" +
                    "<br>" +
                    "Licensed under the Apache License, Version 2.0 (the \"License\");<br>" +
                    "you may not use this file except in compliance with the License.<br>" +
                    "You may obtain a copy of the License at<br>" +
                    "<br>" +
                    "   http://www.apache.org/licenses/LICENSE-2.0<br>" +
                    "<br>" +
                    "Unless required by applicable law or agreed to in writing, software<br>" +
                    "distributed under the License is distributed on an \"AS IS\" BASIS,<br>" +
                    "WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.<br>" +
                    "See the License for the specific language governing permissions and<br>" +
                    "limitations under the License.");


}
*/