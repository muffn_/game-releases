package com.nomuffinlabs.gamereleases.fragments;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;

import com.google.android.gms.common.api.Releasable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.nomuffinlabs.gamereleases.UI.MyAdapter;
import com.nomuffinlabs.gamereleases.activities.MainActivity;
import com.nomuffinlabs.gamereleases.utils.OnGamesLoadedCallback;
import com.nomuffinlabs.gamereleases.utils.OnLoadMoreListener;
import com.nomuffinlabs.gamereleases.handlers.DataHandler;
import com.nomuffinlabs.gamereleases.models.Game;
import com.nomuffinlabs.gamereleases.utils.MyUtils;
import com.nomuffinlabs.gamereleases.R;

import java.util.ArrayList;
import java.util.Objects;

import static com.nomuffinlabs.gamereleases.activities.MainActivity.INSTANCE;
import static com.nomuffinlabs.gamereleases.activities.MainActivity.LISTVIEWER_FAVORITES;
import static com.nomuffinlabs.gamereleases.activities.MainActivity.LISTVIEWER_MAIN;
import static com.nomuffinlabs.gamereleases.activities.MainActivity.LISTVIEWER_POPULAR;
import static com.nomuffinlabs.gamereleases.activities.MainActivity.LISTVIEWER_RELEASED;
import static com.nomuffinlabs.gamereleases.handlers.DataHandler.ALL_GAMES_FETCHED;
import static com.nomuffinlabs.gamereleases.handlers.DataHandler.allGamesFetched;
import static com.nomuffinlabs.gamereleases.handlers.DataHandler.favoritesList;
import static com.nomuffinlabs.gamereleases.utils.MyUtils.debug;

public class ListViewerFragment extends Fragment {

    public ListViewerFragment fragment;

    public MyAdapter mAdapter;
    public RecyclerView mRecyclerView;
    public ViewGroup rootView;
    public FloatingActionButton fab;
    public ProgressBar loadingBar;

    public String instance;
    private ArrayList<Game> list;
    private MainActivity mActivity;

    public ListViewerFragment() { /* Required empty public constructor*/ }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = (ViewGroup) inflater.inflate(R.layout.fragment_list, container, false);
        fragment = this;
        loadingBar = rootView.findViewById(R.id.loading_bar);
        fab = rootView.findViewById(R.id.fab);
        mRecyclerView = rootView.findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        MyUtils.setLayoutManager(getContext(), mRecyclerView);

        assert getArguments() != null;
        instance = getArguments().getString(INSTANCE);
        initInstance();

        mAdapter = new MyAdapter(mActivity, mRecyclerView, list);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnScrollListener(onScrollListener);
        MyUtils.setRecyclerviewStartAnimations(mRecyclerView, mActivity);

        animateFab();
        fab.setBackgroundColor(MyUtils.fetchAccentColor(requireContext()));

        getContent();

        return rootView;
    }


    public void initInstance() {
        debug("listviewer", "list: "+list);
        list = MyUtils.getListByInstance(instance);

        if ( instance.equals(LISTVIEWER_MAIN) || instance.equals(LISTVIEWER_POPULAR) || instance.equals(LISTVIEWER_RELEASED)  ) {
            fab.setOnClickListener(fabScrollUpClickListener);
            fab.setOnClickListener(fabScrollUpClickListener);

        } else if ( instance.equals(LISTVIEWER_FAVORITES) ) {
            fab.setImageResource(R.drawable.ic_check_white_24dp);
            fab.setOnClickListener(fabFavoritesClickListener);

        } else {
            //TODO create snackbar with error message
        }
    }

    public void updateList() {
        mAdapter.notifyDataSetChanged();
        mRecyclerView.invalidate();
    }


    @Override
    public void onResume() {
        super.onResume();

        if ( mRecyclerView != null && mAdapter != null )
            updateList();

        if (DataHandler.orientation_changed) {
            MyUtils.setLayoutManager(getContext(), mRecyclerView);
            DataHandler.orientation_changed = false;

            //TODO why getcontent?
            //getContent();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof MainActivity){
            mActivity = (MainActivity) context;
        }
    }
    
    
    public void getContent() {
        if ( !allGamesFetched ) {
            if (list.size() < 30) {
                DataHandler.loadNextGames(mActivity, onGamesLoadedCallback);

            } else {
                mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                    @Override
                    public void onLoadMore() {
                        DataHandler.loadNextGames(mActivity, onGamesLoadedCallback);
                    }
                });
            }
        }
    }


    OnGamesLoadedCallback onGamesLoadedCallback = new OnGamesLoadedCallback() {
        @Override
        public void onStartLoading() {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showLoading(true);
                }
            });
            mAdapter.isLoading = true;
        }

        @Override
        public void onLoadedCallback(final String result) {
            debug("listviewer", "gamelist size: "+list.size());

            if ( result.equals(ALL_GAMES_FETCHED) ) {
                allGamesFetched = true;
            } else {
                mAdapter.isLoading = false;

                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showLoading(false);
                        Objects.requireNonNull(mActivity).refreshMainView();
                        if ( result.equals(ALL_GAMES_FETCHED) ) {
                            Objects.requireNonNull(mActivity).openSnackbar("All games has been fetched", Snackbar.LENGTH_LONG);
                        }
                    }
                });


                //in case its still fetching the first ones(first case of getcontent is supposed to be a loop)
                //bit ugly but dont have a better solution for now TODO
                if (list.size() < 30) {
                    getContent();
                }
            }
        }

        @Override
        public void onError() {
            //TODO show error
        }
    };


    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if ( dy > 0) {
                fab.hide();
            } else {
                fab.show();
            }
        }
    };

    View.OnClickListener fabScrollUpClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mRecyclerView.smoothScrollToPosition(0);
            fab.hide();
        }
    };


    View.OnClickListener fabFavoritesClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            //check if there are any released games
            boolean favsListContainsReleasedGames = false;
            for ( int i = 0; i < favoritesList.size(); i++) {
                if ( favoritesList.get(i).date < System.currentTimeMillis() )
                    favsListContainsReleasedGames = true;
            }


            //make dialog and ask to remove all released games
            if ( favsListContainsReleasedGames ) {
                new MaterialDialog.Builder(Objects.requireNonNull(getContext(), "Context is null"))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                //remove released games
                                for (int i = 0; i < favoritesList.size(); i++) {
                                    debug("listviewer", "name: " + favoritesList.get(i).name);
                                    debug("listviewer", "current: " + System.currentTimeMillis());
                                    debug("listviewer", "fav_millis: " + favoritesList.get(i).date);

                                    if (favoritesList.get(i).date < System.currentTimeMillis()) {
                                        favoritesList.remove(i);

                                        mAdapter.notifyItemRemoved(i);

                                        i = -1;
                                    }
                                }
                            }
                        })
                        .title("Remove all already released games from your favorites?")
                        .backgroundColor(MyUtils.colorPrimary)
                        .contentColor(MyUtils.primarytextcolor)
                        .positiveText("Yes, please")
                        .negativeText("Nope")
                        .show();
            } else {
                MyUtils.mToast("Currently there aren't any released games to clear in your favorites", false, getContext());
            }

            fab.hide(); //TODO show again if new favorites were added
        }
    };













    public void showLoading(final boolean show) {
        if (show)
            loadingBar.setVisibility(View.VISIBLE);
        else
            loadingBar.setVisibility(View.INVISIBLE);
    }



    protected void animateFab() {

        // Scale down animation
        ScaleAnimation shrink =  new ScaleAnimation(1f, 0.2f, 1f, 0.2f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        shrink.setDuration(150);     // animation duration in milliseconds
        shrink.setInterpolator(new DecelerateInterpolator());
        shrink.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                // Scale up animation
                ScaleAnimation expand =  new ScaleAnimation(0.2f, 1f, 0.2f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                expand.setDuration(100);     // animation duration in milliseconds
                expand.setInterpolator(new AccelerateInterpolator());
                fab.startAnimation(expand);
                fab.show();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        fab.startAnimation(shrink);
    }
}