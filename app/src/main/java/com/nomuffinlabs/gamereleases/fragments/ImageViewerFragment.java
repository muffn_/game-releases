package com.nomuffinlabs.gamereleases.fragments;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.core.content.res.ResourcesCompat;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nomuffinlabs.gamereleases.handlers.ImageHandler;
import com.nomuffinlabs.gamereleases.models.Game;
import com.nomuffinlabs.gamereleases.utils.MyUtils;
import com.nomuffinlabs.gamereleases.R;
import com.nomuffinlabs.gamereleases.utils.SharedPrefHelper;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by Robin Strutz on 30.12.2015.
 */
public class ImageViewerFragment extends Fragment {

    ImageViewerFragment frag;
    public ImageView image;
    public Activity act;

    ArrayList<String> quality_names;
    ArrayList<String> quality_values;

    LinearLayout qualities;
    ViewGroup rootView;
    Toolbar toolbar;
    String id;

    int current_quality = 0;
    public static boolean share_requestPermissions = false;
    public static boolean save_requestPermissions = false;

    PhotoViewAttacher mAttacher;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(
                R.layout.activity_imageviewer, container, false);

        toolbar = rootView.findViewById(R.id.toolbar);
        toolbar.setTitle("");

        frag = this;
        act = getActivity();
        image = rootView.findViewById(R.id.image);
        mAttacher = new PhotoViewAttacher(image);
        qualities = rootView.findViewById(R.id.qualities);

        Bundle extras = getArguments();
        id = extras.getString(Game.KEY_ID);

        quality_names = extras.getStringArrayList("quality_names");
        quality_values = extras.getStringArrayList("quality_values");

        for ( int i = 0; i < quality_names.size(); i++) {

            final Button btn = new Button(act);
            btn.setText(quality_names.get(i));

            final int primarytextcolor = ResourcesCompat.getColor(getResources(), R.color.invert, null);
            final int sectextcolor = ResourcesCompat.getColor(getResources(), R.color.thirdTextColor, null);
            btn.setTextColor(sectextcolor);

            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            p.weight = 1;

            btn.setLayoutParams(p);

            //cover?
            if ( quality_names.size() == 2) {//quality names of cover only has 4 items
                if ( SharedPrefHelper.getString(getResources().getString(R.string.cover_quality_key),
                        getResources().getString(R.string.covers_qualities_default),
                        act).equals(quality_values.get(i)) ) {
                    btn.setTextColor(primarytextcolor);
                    current_quality = i;
                }
            } else {//or screenshots?
                if ( SharedPrefHelper.getString(getResources().getString(R.string.screenshot_quality_key),
                        getResources().getString(R.string.screenshots_qualities_default),
                        act).equals(quality_values.get(i)) ) {
                    btn.setTextColor(primarytextcolor);
                    current_quality = i;
                }
            }

            int[] attrs = new int[]{R.attr.selectableItemBackground};
            TypedArray typedArray = act.obtainStyledAttributes(attrs);
            int backgroundResource = typedArray.getResourceId(0, 0);
            btn.setBackgroundResource(backgroundResource);
            typedArray.recycle();

            final int finalI = i;
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Button last_selected = (Button) qualities.getChildAt(current_quality);
                    last_selected.setTextColor(sectextcolor);
                    btn.setTextColor(primarytextcolor);

                    current_quality = finalI;

                    for ( int n = 0; n < quality_names.size(); n++) {
                        String s1 = quality_names.get(n);
                        String s2 = quality_values.get(n);
                        if ( btn.getText().equals(s1)) {

                        }
                    }
                    loadImage(quality_values.get(current_quality));
                }
            });

            qualities.addView(btn);

        }

        loadImage(quality_values.get(current_quality));

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mAttacher.cleanup();
    }

    @Override
    public void onStart() {
        super.onStart();
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                act.onBackPressed();
            }
        });
    }



    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        debug("onRequestPermissionsResult1");
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Bitmap b = ((BitmapDrawable)image.getDrawable()).getBitmap();
            if ( share_requestPermissions ) {
                MyUtils.shareImage(act, b);
                share_requestPermissions = false;
            } else if ( save_requestPermissions ) {
                MyUtils.saveImageInPictures(act, b);
                save_requestPermissions = false;
            }
        }
    }


    public void loadImage(String quality) {
        //cover or screenshots? count quality values count, kinda dumb TODO
        String url = quality_names.size() == 2 ? ImageHandler.getCoverQualityUrl(id, quality) : ImageHandler.getScreenshotQualityUrl(id, quality);
        Picasso.get().load(url).into(image, new Callback() {
            @Override
            public void onSuccess() {
                rootView.findViewById(R.id.openinbrowser).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if ( image == null )
                            return;
                        MyUtils.openUrlInCustomTab(act, ImageHandler.getScreenshotQualityUrl(id, quality_values.get(current_quality)), Color.DKGRAY);
                    }
                });
                rootView.findViewById(R.id.share).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if ( image == null )
                            return;
                        Bitmap b = ((BitmapDrawable)image.getDrawable()).getBitmap();
                        MyUtils.shareImage(act, b);
                    }
                });
                rootView.findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if ( image == null )
                            return;
                        Bitmap b = ((BitmapDrawable)image.getDrawable()).getBitmap();
                        MyUtils.saveImageInPictures(act, b);
                    }
                });
                if (mAttacher != null) {
                    mAttacher.update();
                } else {
                    mAttacher = new PhotoViewAttacher(image);
                }
            }

            @Override
            public void onError(Exception e) {
            }
        });
    }


    public void debug(String message) {
        Log.e("Fragment_ImageViewer: ", message);
    }

















}
