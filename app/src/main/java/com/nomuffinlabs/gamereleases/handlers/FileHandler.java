package com.nomuffinlabs.gamereleases.handlers;

import android.content.Context;

import com.nomuffinlabs.gamereleases.models.Game;
import com.nomuffinlabs.gamereleases.utils.MyUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import static com.nomuffinlabs.gamereleases.models.Game.KEY_ID;
import static com.nomuffinlabs.gamereleases.models.Game.KEY_NAME;

/**
 * Created by robin on 30.10.2016.
 */

public class FileHandler {

    public static String FILE_FAVORITES = "favorites.txt";

    public static void writeToFile(Context con, String filename, String content) {
        try {
            FileOutputStream outputStream = con.openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(content.getBytes());
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static boolean fileExists(Context context, String filename) {
        File file = context.getFileStreamPath(filename);
        if ( !(file == null || !file.exists() ) ) {
            if ( file.length() == 0 ) {
                file.delete();
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public static File getFile(Context con, String filename) {
        return fileExists(con, filename) ? con.getFileStreamPath(filename) : null;
    }


    public static String readFile(Context context, String filename) {
        File file = context.getFileStreamPath(filename);
        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
        }
        catch (IOException e) {
        }

        return text.toString();
    }

    public static String readFile(Context context, File file) {
        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
        }
        catch (IOException e) {
        }

        return text.toString();
    }






    //                                                                                           ignore releases in favorite section to have earlier released games
    public static void loadListFromFile(Context con, String filename, ArrayList list, int value) {
        if (!FileHandler.fileExists(con, filename)) {
            return;
        }

        String content = FileHandler.readFile(con, filename);
        try {
            JSONArray jarray = new JSONArray(content);
            for (int i = 0; i < jarray.length(); i++) {
                JSONObject c = jarray.getJSONObject(i);
                if (!c.isNull(KEY_ID) && !c.isNull(KEY_NAME)) {
                    Game g = JsonHandler.getGameFromJsonObject(c);
                    if (g != null) {
                        if (!MyUtils.listContains(list, g.id))
                            list.add(g);
                    }
                    break;
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
