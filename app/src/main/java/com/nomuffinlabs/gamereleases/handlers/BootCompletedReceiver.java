package com.nomuffinlabs.gamereleases.handlers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.nomuffinlabs.gamereleases.utils.MyUtils;

/**
 * Created by robin on 09.02.2017.
 */

public class BootCompletedReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        MyUtils.setAlarm(context);
    }
}
