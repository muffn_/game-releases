package com.nomuffinlabs.gamereleases.handlers;

import android.content.Context;

import com.nomuffinlabs.gamereleases.R;
import com.nomuffinlabs.gamereleases.utils.SharedPrefHelper;

public class ImageHandler {

    static String image_url = "https://images.igdb.com/igdb/image/upload/t_";


    public static String getCoverUrl(String cover_id, Context context) {
        String cover_quality_key = context.getResources().getString(R.string.cover_quality_key);
        String quality = SharedPrefHelper.getString(cover_quality_key, context.getResources().getString(R.string.covers_qualities_default), context);
        return image_url + quality + "/" + cover_id + ".jpg";
    }

    public static String getScreenshotUrl(String screenshot_id, Context context) {
        String screenshot_quality = context.getResources().getString(R.string.screenshot_quality_key);
        String quality = SharedPrefHelper.getString(screenshot_quality, context.getResources().getString(R.string.screenshots_qualities_default), context);
        return image_url + quality + "/" + screenshot_id + ".jpg";
    }


    public static String getScreenshotQualityUrl(String screenshot_id, String quality) {
        return image_url + quality + "/" + screenshot_id + ".jpg";
    }

    public static String getCoverQualityUrl(String cover_id, String quality) {
        return image_url + quality + "/" + cover_id + ".jpg";
    }

}
