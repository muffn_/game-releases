package com.nomuffinlabs.gamereleases.handlers;

import com.nomuffinlabs.gamereleases.models.Game;
import com.nomuffinlabs.gamereleases.models.Video;
import com.nomuffinlabs.gamereleases.utils.MyUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

import static com.nomuffinlabs.gamereleases.handlers.DataHandler.gameList;
import static com.nomuffinlabs.gamereleases.handlers.DataHandler.game_categories;
import static com.nomuffinlabs.gamereleases.models.Game.KEY_ARTWORKS_IDS;
import static com.nomuffinlabs.gamereleases.models.Game.KEY_COMPANIES;
import static com.nomuffinlabs.gamereleases.models.Game.KEY_COVER_ID;
import static com.nomuffinlabs.gamereleases.models.Game.KEY_DATE;
import static com.nomuffinlabs.gamereleases.models.Game.KEY_DATE_CATEGORY;
import static com.nomuffinlabs.gamereleases.models.Game.KEY_DATE_ID;
import static com.nomuffinlabs.gamereleases.models.Game.KEY_GAME_CATEGORIES;
import static com.nomuffinlabs.gamereleases.models.Game.KEY_ID;
import static com.nomuffinlabs.gamereleases.models.Game.KEY_NAME;
import static com.nomuffinlabs.gamereleases.models.Game.KEY_PLATFORMS;
import static com.nomuffinlabs.gamereleases.models.Game.KEY_POPULARITY;
import static com.nomuffinlabs.gamereleases.models.Game.KEY_REGION;
import static com.nomuffinlabs.gamereleases.models.Game.KEY_SCREENSHOTS;
import static com.nomuffinlabs.gamereleases.models.Game.KEY_SLUG;
import static com.nomuffinlabs.gamereleases.models.Game.KEY_SUMMARY;
import static com.nomuffinlabs.gamereleases.models.Game.KEY_URL;
import static com.nomuffinlabs.gamereleases.models.Game.KEY_VIDEOS;
import static com.nomuffinlabs.gamereleases.models.Game.KEY_WEBSITES;
import static com.nomuffinlabs.gamereleases.utils.MyUtils.debug;

public class JsonHandler {


    public static Game getGameFromJsonObject(JSONObject c) {

        int date_id = (int) getJsonField(c, KEY_DATE_ID, -1);
        int date_category = (int) getJsonField(c, KEY_DATE_CATEGORY, -1);
        long date = new Long( (int) getJsonField(c, KEY_DATE, -1) );
        ArrayList<String> platform_names = (ArrayList<String>) getJsonField(c, KEY_PLATFORMS, "null");
        int id = (int) getJsonField(c, KEY_ID, -1);
        String cover_id = (String) getJsonField(c, KEY_COVER_ID, "null");
        String name = (String) getJsonField(c, KEY_NAME, "null");
        String summary = (String) getJsonField(c, KEY_SUMMARY, "null");
        float popularity = new Float( "" + getJsonField(c, KEY_POPULARITY, 0) );
        ArrayList<String> company_names = (ArrayList<String>) getJsonField(c, KEY_COMPANIES, "null");
        ArrayList<String> game_category_names = (ArrayList<String>) getJsonField(c, KEY_GAME_CATEGORIES, "null");
        ArrayList<String> screenshot_ids = (ArrayList<String>) getJsonField(c, KEY_SCREENSHOTS, "null");
        String slug = (String) getJsonField(c, KEY_SLUG, "null");
        ArrayList<String> website_urls = (ArrayList<String>) getJsonField(c, KEY_WEBSITES, "null");
        String url = (String) getJsonField(c, KEY_URL, "null");
        int region = (int) getJsonField(c, KEY_REGION, 8);
        ArrayList<String> artworks_ids = (ArrayList<String>) getJsonField(c, KEY_ARTWORKS_IDS, "");

        ArrayList<Video> videos = new ArrayList<>();

        JSONArray obj = null;
        try {
            obj = c.getJSONArray(KEY_VIDEOS);
            for (int i = 0; i < obj.length(); i++) {
                videos.add(
                        new Video(
                            obj.getJSONArray(i).getString(0),
                                obj.getJSONArray(i).getString(1)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return new Game(date_id, date_category, date, platform_names, id,
                cover_id, name, summary, popularity, company_names,
                game_category_names, screenshot_ids, slug, videos,
                website_urls ,url, region, artworks_ids);
    }


    public static void addGamesToList(String jsonRaw, ArrayList<Game> list) {

        debug("JsonHandler", "addgames to list: "+ jsonRaw);
        debug("JsonHandler", "addgames to list: "+ list.size());

        if (jsonRaw != null && !jsonRaw.isEmpty()) {
            try {
                JSONArray jarray = new JSONArray(jsonRaw);

                for (int i = 0; i < jarray.length(); i++) {
                    JSONObject c = jarray.getJSONObject(i);

                    if (!c.isNull(KEY_ID) && !c.isNull(Game.KEY_NAME)) {

                        Game g = getGameFromJsonObject(c);

                        DataHandler.correctFavorites(g);
                        //add to main list
                        if (!MyUtils.listContains(list, g.id)) {
                            gameList.add(g);
                        }

                        //update categories
                        for (String category : g.game_category_names) {
                            if ( !game_categories.contains(category) ) {
                                game_categories.add(category);
                                Collections.sort(game_categories);
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static ArrayList<Object> fillListFromJsonArray(JSONArray array) {

        ArrayList<Object> list = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            try {
                if (!array.isNull(i))
                    list.add(array.get(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public static Object getJsonField(JSONObject jsonObject, String field, Object def) {
        Object obj = def;
        try {
            if (!jsonObject.isNull(field))
                obj = jsonObject.get(field);

            if ( obj != def && obj != null ) {
                if (obj instanceof JSONArray) {
                    JSONArray jarray = (JSONArray) obj;
                    return fillListFromJsonArray(jarray);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }

    public static String getStringFromJsonObject(JSONObject jsonObject, String field, String def) {
        String obj = def;
        try {
            if (!jsonObject.isNull(field))
                obj = jsonObject.getString(field);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }
}
