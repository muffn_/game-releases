package com.nomuffinlabs.gamereleases.handlers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.os.AsyncTask;
import android.os.Build;
import android.os.NetworkOnMainThreadException;

import com.nomuffinlabs.gamereleases.utils.HaveInternetCallback;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import androidx.annotation.NonNull;

import static com.nomuffinlabs.gamereleases.utils.MyUtils.debug;

/**
 * Created by robin on 27.10.2016.
 */

public class InternetHandler {

    Context context;

    public int internet_checking_responseCode;
    public int RESPONSE_CODE_OK = 200;

    public InternetHandler(Context con, HaveInternetCallback mcallback) {
        context = con;
        checkActiveInternetConnection check = new checkActiveInternetConnection();
        check.setOnHaveInternetListener(mcallback);
        check.execute();
    }

    public class checkActiveInternetConnection extends AsyncTask<Void, Void, Void> {

        HaveInternetCallback callback;

        protected void onPreExecute() {
        }

        protected Void doInBackground(Void... params) {
            if (isNetworkAvailable(context)) {
                HttpURLConnection urlc = null;
                try {
                    urlc = (HttpURLConnection) (new URL("https://www.google.com").openConnection());
                    urlc.setRequestProperty("User-Agent", "Test");
                    urlc.setRequestProperty("Connection", "close");
                    urlc.setConnectTimeout(1500);
                    urlc.connect();
                    internet_checking_responseCode = urlc.getResponseCode();
                } catch (NetworkOnMainThreadException | IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            if (internet_checking_responseCode == 200) {
                //have internet
                callback.haveInternetCallback(internet_checking_responseCode);
            } else {
                callback.noInternetCallback(internet_checking_responseCode);
                //have no internet, show error
                //snackbar = Snackbar.make(findViewById(R.id.rootlayout), "No internet connection available", Snackbar.LENGTH_LONG);
                //snackbar.show();
            }
        }

        public void setOnHaveInternetListener(HaveInternetCallback eventListener) {
            callback = eventListener;
        }

    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork()).hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET);
        } else {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null;
        }
    }

    public static boolean hasInternet(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

}
