package com.nomuffinlabs.gamereleases.handlers;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import com.google.android.material.snackbar.Snackbar;
import android.text.Html;
import android.util.Log;

import com.nomuffinlabs.gamereleases.R;
import com.nomuffinlabs.gamereleases.models.Game;
import com.nomuffinlabs.gamereleases.utils.HaveInternetCallback;
import com.nomuffinlabs.gamereleases.utils.MyUtils;
import com.nomuffinlabs.gamereleases.utils.OnGamesLoadedCallback;
import com.nomuffinlabs.gamereleases.utils.SharedPrefHelper;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static com.nomuffinlabs.gamereleases.activities.MainActivity.debug;
import static com.nomuffinlabs.gamereleases.utils.SharedPrefHelper.SHAREDPREF_WANTED_PLATFORMS;

/**
 * Created by robin on 12.10.2016.
 */


//Fetches everything for the overview
public class DataHandler {

    //lists
    public static ArrayList<Game> gameList = new ArrayList<>(); //Contains everything

    public static ArrayList<Game> filteredMainList = new ArrayList<>();
    public static ArrayList<Game> releasedList = new ArrayList<>();
    public static ArrayList<Game> favoritesList = new ArrayList<>();
    public static ArrayList<Game> popularList = new ArrayList<>();

    public static ArrayList<String> game_categories = new ArrayList<>();
    public static ArrayList<String> allPlatforms = new ArrayList<>(Arrays.asList("PC", "PS4", "XONE", "Switch", "Oculus VR", "PlayStation VR", "PlayStation 5", "Xbox Series X"));

    //TODO get new data every 14 days? Should probably be made by the user? Or snackbar with something like "Been 14 days, refreshing data"
    private static final int FILE_EXPIRES = 14;

    //at every loadNextGame function call the offset it gets increased by one
    public static int offset = 0;

    public static boolean orientation_changed = false;

    public static boolean allGamesFetched = false;
    public static boolean cachedGamesLoaded = false;

    public static String ALL_GAMES_FETCHED = "all_games_fetched";

    //Basic urls for getting data
    public static String default_url = "https://ewoks.de/gamereleases/request.php?offset=";

    public static Snackbar snackbar;
    public static void openSnackbar(String text, int time_length, Activity act) {

        int textColor = act.getResources().getColor(R.color.primaryTextColor);
        String hexColor = String.format("#%06X", (0xFFFFFF & textColor));

        //TODO does it need to be reinitialized every time?
        snackbar = Snackbar.make(act.findViewById(R.id.rootlayout), Html.fromHtml("<font color=\""+hexColor+"\">"+text+"</font>"), time_length);
        snackbar.getView().setBackgroundColor(act.getResources().getColor(R.color.navBarColor));
        snackbar.show();
    }
    public static void hideSnackbar() {
        if (snackbar != null)
            if (snackbar.isShown())
                snackbar.dismiss();
    }


    public static boolean running = false;
    public static void loadNextGames(final Activity activity, final OnGamesLoadedCallback callback) {

        if ( !running ) {
            if (!allGamesFetched) {

                debug("loadNextGames");
                running = true;
                callback.onStartLoading();

                new InternetHandler(activity, new HaveInternetCallback() {
                    @Override
                    public void noInternetCallback(int responseCode) {

                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                openSnackbar("No internet connection, trying again...", Snackbar.LENGTH_INDEFINITE, activity);
                            }
                        });

                        final Handler h = new Handler();
                        final int delay = 1000; //milliseconds

                        h.postDelayed(new Runnable() {
                            public void run() {
                                //do something
                                debug("postDelayed");

                                if (InternetHandler.hasInternet(activity)) {
                                    running = false;
                                    DataHandler.loadNextGames(activity, callback);
                                    debug("internet available!");
                                } else {
                                    h.postDelayed(this, delay);
                                    debug("no internet");
                                }

                            }
                        }, delay);
                    }

                    @Override
                    public void haveInternetCallback(int responseCode) {
                        debug("haveInternetCallback");
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideSnackbar();
                            }
                        });

                        new downloadJsonFromUrl(getGamesUrl(), callback, activity.getApplicationContext()).execute();
                    }
                });

            } else {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        callback.onLoadedCallback(ALL_GAMES_FETCHED);
                    }
                });

                Log.e("debug", "all games has been fetched!");
            }
        }
    }


    public static ArrayList<String> getPreferredPlatforms(Context context) {
        String[] plats = new String[allPlatforms.size()];
        plats = allPlatforms.toArray(plats);

        Set<String> set = SharedPrefHelper.getStringSet(SHAREDPREF_WANTED_PLATFORMS, new HashSet<>(Arrays.asList(plats)), context);
        ArrayList<String> list = new ArrayList<>();
        list.addAll(set);
        return list;
    }

    public static void loadCachedGames(Context con) {

        String filename = getGamesUrl().replace(File.separator, ",");

        //getting days since created/modified
        int daysSinceModified = 0;
        if (FileHandler.fileExists(con, filename) ) {
            daysSinceModified = MyUtils.calculateDays(System.currentTimeMillis() - FileHandler.getFile(con, filename).lastModified());
        }

        File dir = con.getFilesDir();
        File[] subFiles = dir.listFiles();

        if (subFiles != null) {

            for (File file : subFiles) {

                String name = file.getName();

                if ( !name.contains("themes") && !name.contains("genres") && !name.contains("favorites") ) {

                    if ( daysSinceModified < FILE_EXPIRES ) {
                        String result = FileHandler.readFile(con, file);
                        JsonHandler.addGamesToList(result, gameList);

                        offset += 1;
                    } else {
                        file.delete();
                    }
                }
            }
        }

        debug("finished loading cached games");
        cachedGamesLoaded = true;

    }


    public static void correctFavorites(Game newGame) {

        //Update outdated data in favorites
        if ( MyUtils.listContains(favoritesList, newGame.id ) ) {

            int pos = MyUtils.getPosById(favoritesList, newGame.id );
            Game oldGame = favoritesList.get(pos);

            if ( newGame != oldGame) {

                favoritesList.remove(pos);
                favoritesList.add(newGame);
                Collections.sort(favoritesList);
            }
        }
    }


    private static class downloadJsonFromUrl extends AsyncTask<String, Integer, String> {

        OnGamesLoadedCallback onGamesLoadedCallback;
        String url;
        Context context;

        public downloadJsonFromUrl(String ur, OnGamesLoadedCallback callback, Context con) {
            onGamesLoadedCallback = callback;
            url = ur;
            this.context = con;
        }

        public String doInBackground(String... params) {
            String result = "";
            try {
                URL newUrl = new URL(url);
                HttpURLConnection connection = (HttpURLConnection) newUrl.openConnection();
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                result = sb.toString();
                connection.disconnect();

            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        public void onPostExecute(final String feed) {
            // TODO: check this.exception

            running = false;
            if (feed.equals("") || feed.isEmpty() ||feed.equals("null") ) {
                onGamesLoadedCallback.onError();
            } else {

                try {
                    if (new JSONArray(feed).length() == 0) {
                        allGamesFetched = true;
                    }
                } catch (JSONException e) {
                    allGamesFetched = true;
                    e.printStackTrace();
                    return;
                }

                if ( feed.equals("[]")) {
                    allGamesFetched = true;
                }

                if ( allGamesFetched ) {
                    debug("all games has been fetched: "+gameList.size());
                    onGamesLoadedCallback.onLoadedCallback(ALL_GAMES_FETCHED);
                    return;
                }

                offset += 1;

                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        JsonHandler.addGamesToList(feed, gameList);
                        MyUtils.updateLists(context);
                        onGamesLoadedCallback.onLoadedCallback(feed);

                    }
                }).start();

            }
        }
    }





    public static String getGamesUrl() {
        String url = default_url;

        url = url + offset;
        debug("url: "+url);
        return url;
    }


/*    public static String getFavoritesAsJson() {
        ArrayList list = MainActivity.favoritesList;

        JSONArray jarray = new JSONArray();

        for (int i = 0; i < list.size(); i++) {
            Game g = (Game) list.get(i);
            if (g == null)
                continue;

            *//*ArrayList<String> screenshots, ArrayList<String> videos, ArrayList<String> websites, ArrayList<Integer> myPlatforms) {*//*

            JSONObject c = new JSONObject();
            try {
                c.put(KEY_ID, g.id);
                c.put(KEY_NAME, g.title);
                c.put(KEY_URL, g.url);
                c.put(KEY_COVER, new JSONObject().put(KEY_COVER_ID, g.cover_id));
                c.put(KEY_SUMMARY, g.summary);
                c.put(KEY_FIRST_RELEASE_DATE, g.first_release_date);
                c.put(KEY_DEVELOPERS, new JSONArray(g.devs));
                c.put(KEY_PUBLISHERS, new JSONArray(g.pubs));
                c.put(KEY_GAME_MODES, new JSONArray(g.game_modes));
                c.put(KEY_KEYWORDS, new JSONArray(g.keywords));
                c.put(KEY_THEMES, new JSONArray(g.themes));
                c.put(KEY_GENRES, new JSONArray(g.genres));
                c.put(KEY_SCREENSHOTS, new JSONArray(g.screenshots));
                c.put(KEY_VIDEOS, new JSONArray(g.videos));
                c.put(KEY_WEBSITES, new JSONArray(g.websites));

                JSONArray devsArray = new JSONArray();
                for (int n = 0; n < g.devs.size(); n++) {
                    Container obj = g.devs.get(n);
                    JSONObject o = new JSONObject();
                    o.put("id", obj.id);
                    o.put("name", obj.name);
                    o.put("url", obj.url);
                    devsArray.put(o);
                }
                c.put(KEY_DEVELOPERS, devsArray);

                JSONArray pubsArray = new JSONArray();
                for (int n = 0; n < g.pubs.size(); n++) {
                    Container obj = g.pubs.get(n);
                    JSONObject o = new JSONObject();
                    o.put("id", obj.id);
                    o.put("name", obj.name);
                    o.put("url", obj.url);
                    pubsArray.put(o);
                }
                c.put(KEY_PUBLISHERS, pubsArray);

                JSONArray keywordsArray = new JSONArray();
                for (int n = 0; n < g.keywords.size(); n++) {
                    Container obj = g.keywords.get(n);
                    JSONObject o = new JSONObject();
                    o.put("id", obj.id);
                    o.put("name", obj.name);
                    o.put("url", obj.url);
                    keywordsArray.put(o);
                }
                c.put(KEY_KEYWORDS, keywordsArray);

                JSONArray enginesArray = new JSONArray();
                for (int n = 0; n < g.engines.size(); n++) {
                    GameEngine obj = g.engines.get(n);
                    JSONObject o = new JSONObject();
                    o.put("id", obj.id);
                    o.put("name", obj.name);
                    o.put("url", obj.url);
                    o.put("platforms", new JSONArray(obj.platforms));
                    enginesArray.put(o);
                }
                c.put(KEY_GAME_ENGINES, enginesArray);

                JSONArray releaseDatesArray = new JSONArray();
                for (int n = 0; n < g.releaseDates.size(); n++) {
                    ReleaseDate date = g.releaseDates.get(n);
                    JSONObject o = new JSONObject();
                    o.put("category", date.getCategory());
                    o.put("platform", date.getPlatform());
                    o.put("date", date.getDate());
                    o.put("region", date.getRegion());
                    releaseDatesArray.put(o);
                }
                c.put(KEY_RELEASE_DATES, releaseDatesArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            jarray.put(c);
        }

        String result = "";
        if (jarray.length() > 0) {
            result = jarray.toString();
        }
        return result;
    }*/








}
