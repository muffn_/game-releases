package com.nomuffinlabs.gamereleases.handlers;

import android.content.Context;
import java.util.Collections;
import static com.nomuffinlabs.gamereleases.handlers.DataHandler.favoritesList;

public class FavoritesHandler {


    public static void loadFavorites(Context context) {

        FileHandler.loadListFromFile(context, FileHandler.FILE_FAVORITES, favoritesList, 0);
        Collections.sort(favoritesList);

    }
}
