package com.nomuffinlabs.gamereleases.handlers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.nomuffinlabs.gamereleases.models.Game;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;


/**
 * Created by robin on 13.11.2016.
 */

public class AlarmNotificationsReceiver extends BroadcastReceiver {

    public int DAYS_1 = 1000 * 60 * 60 * 24; //millis * seconds * minutes * hours = 1 day
    public int DAYS_3 = DAYS_1 * 3;
    public int DAYS_7 = DAYS_1 * 7;
    public int DAYS_30 = DAYS_1 * 30;

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.e("debug", "onreceive");

        Calendar rightNow = Calendar.getInstance();
        int currentHour = rightNow.get(Calendar.HOUR_OF_DAY);

        if (currentHour != 17)
            return;

        ArrayList<Game> favorites = new ArrayList<>();
        FileHandler.loadListFromFile(context, FileHandler.FILE_FAVORITES, favorites, 0);
        Collections.sort(favorites);

        long currentmillis = System.currentTimeMillis() - 86400000; // - 1day

        //going through favorites
        /*for (int i = 0; i < favorites.size(); i++) { TODO

            ArrayList<ReleaseDate> upcomingGameReleases = new ArrayList<>();

            Game game = favorites.get(i);

            //getting all releasedates from game
            for (int n = 0; n < game.releaseDates.size(); n++) {
                long millis_releasedate = game.releaseDates.get(n).getDate();
                int platform = game.releaseDates.get(n).getPlatform();

                //if later than current time and in current platforms
                if (millis_releasedate > currentmillis && platform_ids.contains(platform)) {


                    //check if already in list
                    boolean inList = false;
                    for (int t = 0; t < upcomingGameReleases.size(); t++) {
                        if (millis_releasedate == upcomingGameReleases.get(t).date2) {
                            inList = true;
                            upcomingGameReleases.get(t).list_platform.add(platform);
                        }
                    }
                    if (!inList)
                        upcomingGameReleases.add(new ReleaseDate(millis_releasedate, new ArrayList<>(Arrays.asList(platform))));


                }
            }


            //going through picked up releasedates that'll release in span
            for (int t = 0; t < upcomingGameReleases.size(); t++) {

                long game_date_millis = upcomingGameReleases.get(t).date2;

                if (game_date_millis > currentmillis) {
                    String text = "";
                    ArrayList<Integer> platforms = upcomingGameReleases.get(t).list_platform;

                    int daysDiff = Math.round(MyUtils.getDaysLeft(game_date_millis, currentmillis));
                    debug("name: " + game.title + "   daysDiff: " + daysDiff+"          game millis: "+game_date_millis);
                    if (daysDiff == 0) {
                        text = text + "Today on ";
                    } else if (daysDiff == 1) {
                        text = text + "Tomorrow on ";
                    } else if (daysDiff == 3) {
                        text = text + "In Three days on ";
                    } else if (daysDiff == 7) {
                        text = text + "In Seven days on ";
                    } else if (daysDiff == 30) {
                        text = text + "In 30 days on ";
                    }

                    if (!text.isEmpty()) {
                        String string_platform = "";
                        for (int y = 0; y < platforms.size(); y++) {
                            if (platforms.size() == 1)
                                string_platform = string_platform + MyUtils.getPlatformName(platforms.get(y));
                            else if (y == platforms.size() - 1)
                                string_platform = string_platform + "and " + MyUtils.getPlatformName(platforms.get(y));
                            else if (y == platforms.size() - 2)
                                string_platform = string_platform + MyUtils.getPlatformName(platforms.get(y)) + " ";
                            else
                                string_platform = string_platform + MyUtils.getPlatformName(platforms.get(y)) + ", ";
                        }

                        text = text + string_platform;

                        NotificationCompat.Builder mBuilder =
                                new NotificationCompat.Builder(context.getApplicationContext(), game.id+"");
                        Intent resultIntent = new Intent(context, MainActivity.class);
                        resultIntent.putExtra(INTENT_EXTRA_NOTIFICATION, true);
                        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                        PendingIntent resultPendingIntent =
                                PendingIntent.getActivity(
                                        context,
                                        MainActivity.NOTIFICATION_INTENT,
                                        resultIntent,
                                        PendingIntent.FLAG_UPDATE_CURRENT
                                );

                        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
                        bigText.bigText(text);
                        bigText.setBigContentTitle(game.getTitle());

                        mBuilder.setContentIntent(resultPendingIntent);
                        mBuilder.setSmallIcon(R.drawable.ic_info_white_24dp);
                        mBuilder.setContentTitle(game.getTitle());
                        mBuilder.setContentText(text);
                        mBuilder.setPriority(Notification.PRIORITY_DEFAULT);
                        mBuilder.setStyle(bigText);

                        NotificationManager mNotificationManager =
                                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            NotificationChannel channel = new NotificationChannel(game.id+"",
                                    game.getTitle(),
                                    NotificationManager.IMPORTANCE_DEFAULT);
                            mNotificationManager.createNotificationChannel(channel);
                        }

                        mNotificationManager.notify(game.id, mBuilder.build());

                    }
                }
            }
        }*/
    }

}
