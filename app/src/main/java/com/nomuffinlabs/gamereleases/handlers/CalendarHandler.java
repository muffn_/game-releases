package com.nomuffinlabs.gamereleases.handlers;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import com.google.android.material.snackbar.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.Events;
import com.nomuffinlabs.gamereleases.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by robin on 27.09.2016.
 */

public class CalendarHandler {

    public final static int REQUEST_ACCOUNT_PICKER = 1000;
    public final static int REQUEST_AUTHORIZATION = 1001;
    public final static int REQUEST_GOOGLE_PLAY_SERVICES = 1002;
    final static int REQUEST_PERMISSION_GET_ACCOUNTS = 1003;

    public static ProgressDialog mProgress;
    public static GoogleAccountCredential mCredential;
    public static String[] SCOPES = { CalendarScopes.CALENDAR };

    public static Activity act;

    public CalendarHandler(Activity act2) {
        act = act2;

        mProgress = new ProgressDialog(act);
        mProgress.setMessage("Calling Google Calendar API ...");
        mProgress.setCancelable(false);
        mCredential = GoogleAccountCredential.usingOAuth2(
                act, Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff());
    }

    public static void getResultsFromApi() {
        if (! isGooglePlayServicesAvailable()) {
            acquireGooglePlayServices();
        } else if (mCredential.getSelectedAccountName() == null) {
            chooseAccount();
        } else if (! isDeviceOnline()) {
            mToast("No network connection available.");
        } else {
            debug("5");
            new MakeRequestTask(mCredential).execute();
        }
    }

    @AfterPermissionGranted(REQUEST_PERMISSION_GET_ACCOUNTS)
    public static void chooseAccount() {
        if (EasyPermissions.hasPermissions(act, android.Manifest.permission.GET_ACCOUNTS)) {
            debug("11");
            String accountName = act.getPreferences(Context.MODE_PRIVATE)
                    .getString("accountName", null);
            if (accountName != null) {
                mCredential.setSelectedAccountName(accountName);
                getResultsFromApi();
            } else {
                // Start a dialog from which the user can choose an account
                act.startActivityForResult(
                        mCredential.newChooseAccountIntent(),
                        REQUEST_ACCOUNT_PICKER);
            }
        } else {
            debug("22");
            // Request the GET_ACCOUNTS permission via a user dialog
            EasyPermissions.requestPermissions(
                    act,
                    "This app needs to access your Google account (via Contacts).",
                    REQUEST_PERMISSION_GET_ACCOUNTS,
                    android.Manifest.permission.GET_ACCOUNTS);
        }
    }

    public static boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) act.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    public static boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(act);
        return connectionStatusCode == ConnectionResult.SUCCESS;
    }

    public static void acquireGooglePlayServices() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(act);
        if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
        }
    }
    public static void showGooglePlayServicesAvailabilityErrorDialog(final int connectionStatusCode) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = apiAvailability.getErrorDialog(
                act,
                connectionStatusCode,
                REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }



    public static class MakeRequestTask extends AsyncTask<Void, Void, Void> {

        private com.google.api.services.calendar.Calendar mService = null;
        private Exception mLastError = null;
        private String title;
        private long date;
        private Event event;

        public MakeRequestTask(GoogleAccountCredential credential) {
            HttpTransport transport = AndroidHttp.newCompatibleTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            mService = new com.google.api.services.calendar.Calendar.Builder(
                    transport, jsonFactory, credential)
                    .setApplicationName("Muffin")
                    .build();
            //TODO FIX
            //title = Activity_Gameview.title;
            //date = Activity_Gameview.firstDate;
        }

        @Override
        protected void onPreExecute() {
            debug("");
            CalendarHandler.mProgress.show();
        }

        /**
         * Background task to call Google Calendar API.
         * @param params no parameters needed for this task.
         */
        @Override
        protected Void doInBackground(Void... params) {
            try {
                event = new Event()
                        .setSummary(title)
                        .setDescription("The game "+title+" releases.");

                String year = "";
                String month = "";
                String day = "";

                day = String.valueOf(Integer.parseInt(day) - 1);

                if ( month.length() == 1) {
                    month = "0" + month;
                }

                if ( day.length() == 1) {
                    day = "0" + day;
                }

                debug("year: "+year);
                debug("month: "+month);
                debug("day: "+day);

                DateTime startDateTime = new DateTime(year+"-"+month+"-"+day+"T18:00:00-18:00");
                EventDateTime start = new EventDateTime()
                        .setDateTime(startDateTime);
                event.setStart(start);

                DateTime endDateTime = new DateTime(year+"-"+month+"-"+day+"T18:00:00-18:00");
                EventDateTime end = new EventDateTime()
                        .setDateTime(endDateTime);
                event.setEnd(end);

                String calendarId = "primary";
                event = mService.events().insert(calendarId, event).execute();
                debug("Event created: %s\n"+ event.getHtmlLink());

                /*Calendar service = new Calendar.Builder(httpTransport, jsonFactory, credentials)
                        .setApplicationName("applicationName").build();

                String pageToken = null;
                do {
                    Events events = service.events().alarmList("primary").setPageToken(pageToken).execute();
                    List<Event> items = events.getItems();
                    for (Event e : items) {
                        System.out.println(e.getSummary());
                    }
                    pageToken = events.getNextPageToken();
                } while (pageToken != null);*/

                /*EventReminder[] reminderOverrides = new EventReminder[] {
                        new EventReminder().setMethod("email").setMinutes(24 * 60),
                        new EventReminder().setMethod("popup").setMinutes(10),
                };
                Event.Reminders reminders = new Event.Reminders()
                        .setUseDefault(false)
                        .setOverrides(Arrays.asList(reminderOverrides));
                event.setReminders(reminders);*/
            } catch (Exception e) {
                mLastError = e;
                cancel(true);
            }
            return null;
        }

        /**
         * Fetch a alarmList of the next 10 events from the primary calendar.
         * @return List of Strings describing returned events.
         * @throws IOException
         */
        private List<String> getDataFromApi() throws IOException {
            // List the next 10 events from the primary calendar.
            DateTime now = new DateTime(System.currentTimeMillis());
            List<String> eventStrings = new ArrayList<String>();
            Events events = mService.events().list("primary")
                    .setMaxResults(10)
                    .setTimeMin(now)
                    .setOrderBy("startTime")
                    .setSingleEvents(true)
                    .execute();
            List<Event> items = events.getItems();

            for (Event event : items) {
                DateTime start = event.getStart().getDateTime();
                if (start == null) {
                    // All-day events don't have start times, so just use
                    // the start date.
                    start = event.getStart().getDate();
                }
                eventStrings.add(
                        String.format("%s (%s)", event.getSummary(), start));
            }
            return eventStrings;
        }

        @Override
        protected void onPostExecute(Void output) {
            CalendarHandler.mProgress.hide();

            Snackbar snackbar = Snackbar
                    .make(act.findViewById(R.id.rootlayout), "Event added to your calendar", Snackbar.LENGTH_LONG)
                    .setAction("Open", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //TODO FIX with date. act_favorites.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("content://com.android.calendar/time/"+System.currentTimeMillis()+Activity_Gameview.firstDate)));
                        }
                    });
            snackbar.show();

            /*if (output == null || output.size() == 0) {
                debug("No results returned.");
            } else {
                output.add(0, "Data retrieved using the Google Calendar API:");
                debug(TextUtils.join("\n", output));
            }*/
        }

        @Override
        protected void onCancelled() {
            CalendarHandler.mProgress.hide();
            if (mLastError != null) {
                if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                    CalendarHandler.showGooglePlayServicesAvailabilityErrorDialog(
                            ((GooglePlayServicesAvailabilityIOException) mLastError)
                                    .getConnectionStatusCode());
                } else if (mLastError instanceof UserRecoverableAuthIOException) {
                    act.startActivityForResult(
                            ((UserRecoverableAuthIOException) mLastError).getIntent(),
                            CalendarHandler.REQUEST_AUTHORIZATION);
                } else {
                    debug("The following error occurred:\n"
                            + mLastError.getMessage());
                }
            } else {
                debug("Request cancelled.");
            }
        }
    }







    public static void debug(String message) {
        Log.e("CalendarHandler: ", message);
    }
    public static void mToast(String message) { Log.e("CalendarHandler", "" + message);  Toast toast = new Toast(act); Toast.makeText(act, message, Toast.LENGTH_SHORT).show(); }

}

