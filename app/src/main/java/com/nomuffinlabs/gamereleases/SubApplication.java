package com.nomuffinlabs.gamereleases;

import android.app.Application;

import com.nomuffinlabs.gamereleases.utils.MyUtils;
import com.squareup.picasso.Picasso;

public class SubApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        MyUtils.setNightTheme(getApplicationContext());
        Picasso.setSingletonInstance(new Picasso.Builder(this).build());

    }
}
