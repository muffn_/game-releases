package com.nomuffinlabs.gamereleases.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Robin Strutz on 31.08.2015.
 */
public class SharedPrefHelper {

    public static final String SHAREDPREF_WANTED_PLATFORMS = "wantedPlatforms";
    public static final String SHAREDPREF_MISSING_GAMES_NOTIFICATIONS = "missingGamesNotifications";

    private static SharedPreferences sharedPref;
    private static SharedPreferences.Editor editor;
    static Boolean bool;
    static int in;
    static String string;

    public static SharedPreferences getSharedPref() {
        return sharedPref;
    }

    public static Boolean getBool(String value, Boolean def, Context act) {
        sharedPref = PreferenceManager.getDefaultSharedPreferences(act);
        editor = sharedPref.edit();
        return bool = sharedPref.getBoolean(value, def);
    }
    public static int getInt(String value, Context act) {
        sharedPref = PreferenceManager.getDefaultSharedPreferences(act);
        editor = sharedPref.edit();
        return in = sharedPref.getInt(value, 0);
    }
    public static String getString(String value,  String def, Context act) {
        sharedPref = PreferenceManager.getDefaultSharedPreferences(act);
        editor = sharedPref.edit();
        return string = sharedPref.getString(value, def);
    }

    public static Set<String> getStringSet(String value, Set<String> def, Context act) {
        sharedPref = PreferenceManager.getDefaultSharedPreferences(act);
        editor = sharedPref.edit();
        return sharedPref.getStringSet(value, def);
    }




    public static void setBool(String value, Boolean setTo, Context act) {
        sharedPref = PreferenceManager.getDefaultSharedPreferences(act);
        editor = sharedPref.edit();
        editor.putBoolean(value, setTo).apply();
    }
    public static void setInt(String value, int setTo, Context act) {
        sharedPref = PreferenceManager.getDefaultSharedPreferences(act);
        editor = sharedPref.edit();
        editor.putInt(value, setTo).apply();
    }
    public static void setString(String value, String setTo, Context act) {
        sharedPref = PreferenceManager.getDefaultSharedPreferences(act);
        editor = sharedPref.edit();
        editor.putString(value, setTo).apply();
    }



    public static void setStringSet(String value, String[] setTo, Context act) {

        Set<String> set = new HashSet<>(Arrays.asList(setTo));

        sharedPref = PreferenceManager.getDefaultSharedPreferences(act);
        editor = sharedPref.edit();

        editor.putStringSet(value, set).apply();
    }




    public static void setIntArray(String value, int[] setTo, Context act) {
        String strArr = "";
        for (int anArray : setTo) {
            strArr += anArray + ",";
        }
        strArr = strArr.substring(0, strArr.length() -1); // get rid of last comma

        sharedPref = PreferenceManager.getDefaultSharedPreferences(act);
        editor = sharedPref.edit();

        editor.putString(value, strArr).apply();
    }

    public static int[] getIntArray(String value, Context act) {
        sharedPref = PreferenceManager.getDefaultSharedPreferences(act);
        editor = sharedPref.edit();

        String str = sharedPref.getString(value, null);
        if ( str == null )
            str = "6,49,48,130,37";

        String[] strArr = str.split(",");

        int[] array = new int[strArr.length];
        for (int i=0; i<strArr.length; i++) {
            array[i] = new Integer(strArr[i]);
        }

        return array;
    }

}
