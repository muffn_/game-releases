package com.nomuffinlabs.gamereleases.utils;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.browser.customtabs.CustomTabsIntent;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.content.res.ResourcesCompat;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.nomuffinlabs.gamereleases.R;
import com.nomuffinlabs.gamereleases.handlers.AlarmNotificationsReceiver;
import com.nomuffinlabs.gamereleases.handlers.DataHandler;
import com.nomuffinlabs.gamereleases.models.Game;
import com.nomuffinlabs.gamereleases.UI.FastStaggeredGridLayoutManager;
import com.nomuffinlabs.gamereleases.fragments.ImageViewerFragment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import static android.content.Context.ALARM_SERVICE;
import static com.nomuffinlabs.gamereleases.activities.MainActivity.LISTVIEWER_FAVORITES;
import static com.nomuffinlabs.gamereleases.activities.MainActivity.LISTVIEWER_MAIN;
import static com.nomuffinlabs.gamereleases.activities.MainActivity.LISTVIEWER_POPULAR;
import static com.nomuffinlabs.gamereleases.activities.MainActivity.LISTVIEWER_RELEASED;
import static com.nomuffinlabs.gamereleases.handlers.DataHandler.favoritesList;
import static com.nomuffinlabs.gamereleases.handlers.DataHandler.filteredMainList;
import static com.nomuffinlabs.gamereleases.handlers.DataHandler.gameList;
import static com.nomuffinlabs.gamereleases.handlers.DataHandler.popularList;
import static com.nomuffinlabs.gamereleases.handlers.DataHandler.releasedList;

/**
 * Created by robin on 04.01.2016.
 */
public class MyUtils {

    public static int primarytextcolor, colorPrimary;

    public static void debug(String instance, String message) {
        Log.e("debug, " + instance, message);
    }

    public static Boolean hasLollipop() {
        return android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP;
    }

    public static void setNightTheme(Context context) {
        if ( SharedPrefHelper.getBool("theme_dark", true, context) ) {
            AppCompatDelegate.setDefaultNightMode(
                    AppCompatDelegate.MODE_NIGHT_YES);
        } else {
            AppCompatDelegate.setDefaultNightMode(
                    AppCompatDelegate.MODE_NIGHT_NO);
        }
    }

    public static void setThemeWithAccent(Activity context) {
        colorPrimary = ResourcesCompat.getColor(context.getResources(), R.color.colorPrimary, null);
        primarytextcolor = ResourcesCompat.getColor(context.getResources(), R.color.primaryTextColor, null);

        String s = SharedPrefHelper.getString("accent_color_key", "Blue", context);
        int resourceId = context.getResources().
                getIdentifier(s, "style", context.getPackageName());

        context.setTheme(resourceId);
    }

    public static boolean hasDarkTheme(Context act) {
        return SharedPrefHelper.getBool("theme_dark", true, act);
    }

    public static int darker (int color, float factor) {
        int a = Color.alpha(color);
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);

        return Color.argb( a,
                Math.max( (int)(r * factor), 0 ),
                Math.max( (int)(g * factor), 0 ),
                Math.max( (int)(b * factor), 0 ) );
    }

    public static long millisToDays(long i) {
        return Math.abs(i / (24 * 60 * 60 * 1000));
    }
    public static long daysToMillis(long i) {
        return Math.abs(i * (24 * 60 * 60 * 1000));
    }

    public static View setMargins (View v, int l, int t, int r, int b) {
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(l, t, r, b);
            v.requestLayout();
        }
        return v;
    }



    public static ArrayList<Game> getListByInstance(String instance) {
        if ( instance.equals(LISTVIEWER_MAIN) ) {
            return filteredMainList;
        } else if ( instance.equals(LISTVIEWER_RELEASED) ) {
            return releasedList;
        } else if ( instance.equals(LISTVIEWER_POPULAR) ) {
            return popularList;
        } else if ( instance.equals(LISTVIEWER_FAVORITES) ) {
            return favoritesList;
        } else {
            return null;
        }
    }


    public static boolean hasPreferredPlatforms(Game g, Context context) {
        ArrayList<String> preferredPlatforms = DataHandler.getPreferredPlatforms(context);
        for (String platform : g.platform_names )
            if (preferredPlatforms.contains(platform))
                return true;
        return false;
    }

    public static void updateLists(Context context) {

        for ( int i = 0; i < gameList.size(); i++) {

            Game g = gameList.get(i);

            if ( SharedPrefHelper.getBool("hide_wno_cover", false, context) ) {
                if ( g.cover_id.isEmpty() || g.cover_id.equals("null") ) {
                    continue;
                }
            }

            if ( MyUtils.getDaysLeft(g.date * 1000) < 0 ) {
                if ( !MyUtils.listContains(releasedList, g.id) )
                    if ( hasPreferredPlatforms(g, context) )
                            releasedList.add(g);

            } else {
                if ( !MyUtils.listContains(filteredMainList, g.id) ) {
                    if ( hasPreferredPlatforms(g, context) ) {
                        filteredMainList.add(g);
                        if (g.popularity > 10)
                            popularList.add(g);
                    }
                }
            }
        }

        Collections.sort(filteredMainList);
        Collections.sort(releasedList, Collections.reverseOrder());
        Collections.sort(popularList);

        //TODO refresh potential game view pager adapter
    }

    public static boolean listContains(ArrayList<Game> list, int id) {
        for ( int i = 0; i < list.size(); i++) {
            Game g  = list.get(i);
            if ( g != null ) {
                if (g.id == id) {
                    return true;
                }
            }
        }
        return false;
    }


    public static String DATE_FORMAT_ddMMyyyy = "dd/MM/yyyy";
    public static String DATE_FORMAT_MMMdyyyy = "MMM d, yyyy";
    public static String DATE_YEAR = "yyyy";
    public static String DATE_MONTH = "MM";
    public static String DATE_DAY = "dd";
    public static String getFormattedDate(long millis, String format) {
        String newDate = "";
        if ( millis > 0 ) {
            Date date = new Date(millis);

            //TODO custom date FORMAT via sharedpref    here             \/
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
            newDate = simpleDateFormat.format(date);
            //TODO check if string length is long enough
            if ( !(format.equals(DATE_YEAR) || format.equals(DATE_MONTH) || format.equals(DATE_DAY)) ) {
                if (newDate.substring(0, 5).equals("31/12")) {
                    newDate = newDate.substring(6, 10);
                }
            }
        }
        return newDate;
    }

    public static float getDaysLeft(long millisTo, long millisFrom) {
        Calendar calCurr = Calendar.getInstance();
        calCurr.setTimeInMillis(millisFrom);

        Calendar day = Calendar.getInstance();
        day.setTimeInMillis(millisTo);

        final long millis = day.getTimeInMillis() - calCurr.getTimeInMillis();
        // Convert to days
        return millis / 86400000;// (24 * 60 * 60 * 1000)
    }

    public static int getDaysLeft(long millis) {
        Calendar calCurr = Calendar.getInstance();
        Calendar day = Calendar.getInstance();
        day.setTimeInMillis(millis);
        int daysleft = 0;

        if ( day.get(Calendar.MONTH) == calCurr.get(Calendar.MONTH)) {
            daysleft = day.get(Calendar.DAY_OF_MONTH) - calCurr.get(Calendar.DAY_OF_MONTH);
        } else {
            daysleft = (int) ( ( day.getTimeInMillis()-calCurr.getTimeInMillis() ) / 86400000);
        }

        return daysleft;
        //return (int) ((millis - System.currentTimeMillis()) / (1000*60*60*24));
    }

    public static String getDaysLeftString(long millis) {
        int daysleft = MyUtils.getDaysLeft(millis);

        String date = "";

        if ( MyUtils.getFormattedDate(millis, MyUtils.DATE_FORMAT_ddMMyyyy).length() == 4 ) { //2017
            date = MyUtils.getFormattedDate(millis, MyUtils.DATE_FORMAT_ddMMyyyy);
        } else if (daysleft == 0) {
            date = "Today";
        } else if (daysleft == 1) {
            date = "Tomorrow";
        } else if (daysleft == -1) {
            date = "Yesterday";
        } else if (daysleft < 0) {
            date = "" + Math.abs(daysleft) + " days ago";
        } else {
            date = "" + daysleft + " days";
        }
        return date;
    }

    public static String getWeeksLeftString(long millis) {
        int daysleft = MyUtils.getDaysLeft(millis);

        String date = "";

        if ( MyUtils.getFormattedDate(millis, MyUtils.DATE_FORMAT_ddMMyyyy).length() == 4 ) { //2017
            date = MyUtils.getFormattedDate(millis, MyUtils.DATE_FORMAT_ddMMyyyy);
        } else if (daysleft == 0) {
            date = "Today";
        } else if (daysleft == 1) {
            date = "Tomorrow";
        } else if (daysleft == -1) {
            date = "Yesterday";
        } else if (daysleft < 0) {
            date = "Released";
        } else if (daysleft < 15) {
            date = "" + daysleft + " days";
        } else {
            date = "~ " + Math.round(daysleft/7) + " weeks";
        }
        return date;
    }

    public static int calculateDays(long millis) {
        return (int) millis / (1000*60*60*24);
    }



    public static Snackbar snackbar;
    public static Snackbar showSnackbar(View coordinatorLayout, String message, String actionText, int time, View.OnClickListener onClickListener) {
        return Snackbar.make(coordinatorLayout, message, time)
                .setAction(actionText, onClickListener);
    }
    public static void dismissSnackbar() {
        if ( snackbar != null)
            snackbar.dismiss();
    }


/*    public static int isReleaseDateAlreadyInList(ArrayList<ReleaseDate> list, ReleaseDate newReleaseDate) {
        //-5 == not in alarmList, -2 == in alarmList, i == in alarmList but older -> Replace it with pos
        for (int i = 0; i < list.size(); i++) {
            ReleaseDate list_date = list.get(i);
            if ( list_date.getPlatform() == newReleaseDate.getPlatform()) {
                if ( list_date.getDate() == newReleaseDate.getDate()) {
                    return -2;
                } else if ( list_date.getDate() >  newReleaseDate.getDate() ) {
                    return i;
                }
            }
        }
        return -5;
    }*/

/*    public static int getReleaseDatePosByPlatform(ArrayList<ReleaseDate> list, int platform) {
        int position = 0;
        for (int i = 0; i < list.size(); i++) {
            ReleaseDate list_date = list.get(i);
            if ( list_date.getPlatform() == platform ) {
                return position;
            }
        }
        return -1;
    }*/

    public static int getPosById(ArrayList<Game> list, int id) {

        int result = -1;
        for(int i = 0; i < list.size(); i++) {
            if ( id == list.get(i).id) {
                result = i;
                break;
            }
        }
        return result;
    }



    public static void setAlarm(Context context) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 17); //TODO should be editable
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        Intent intent1 = new Intent(context, AlarmNotificationsReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am = (AlarmManager) context.getSystemService(ALARM_SERVICE);
        am.cancel(pendingIntent);

        am.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);

    }

    public static int getStyledAttribute(Context context, int attr) {
        TypedValue typedValue = new TypedValue();
        TypedArray a = context.obtainStyledAttributes(typedValue.data, new int[] { attr });
        int color = a.getColor(0, 0);
        a.recycle();

        return color;
    }

    public static void mToast(String message, boolean length_long, Context context) {
        Toast toast = new Toast(context);
        int length = length_long ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT;
        Toast.makeText(context, message, length).show();
    }


    /*public static long getCurrentTimeInMillisMinusDays() {
        long days = (long) 86400000 * 14;
        long currentMillis = System.currentTimeMillis();
        if ( SharedPrefHelper.getBool("hide_released", true, DataHandler.context) )
            return currentMillis + (long) 86400000 * 2; //two days for correction
        else
            return currentMillis - days;
    }*/

    public static void openUrlInCustomTab(Context context, String url, int toolbarColor) {
        try {
            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
            builder.setToolbarColor(toolbarColor);
            CustomTabsIntent customTabsIntent = builder.build();
            customTabsIntent.launchUrl(context, Uri.parse(url));
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            context.startActivity(myIntent);
        }
    }

    public static void setLayoutManager(Context context, RecyclerView recyclerView) {

        FastStaggeredGridLayoutManager gridLayoutManager = new FastStaggeredGridLayoutManager(2, FastStaggeredGridLayoutManager.VERTICAL);
        FastStaggeredGridLayoutManager gridLayoutManagerLandscape = new FastStaggeredGridLayoutManager(3, FastStaggeredGridLayoutManager.VERTICAL);

        LinearLayoutManager listLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        LinearLayoutManager listLayoutManagerLandscape =  new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);

        if (SharedPrefHelper.getBool("grid_view", true, context)) {
            if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                recyclerView.setLayoutManager(gridLayoutManagerLandscape);
            } else {
                recyclerView.setLayoutManager(gridLayoutManager);
            }
        } else {
            if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                recyclerView.setLayoutManager(listLayoutManagerLandscape);
            } else {
                recyclerView.setLayoutManager(listLayoutManager);
            }
        }
        recyclerView.invalidate();
    }

    public static void showDialog(Context context, String title, String message,
                                  String positiveText, MaterialDialog.SingleButtonCallback positiveClickListener,
                                  String negativeText, MaterialDialog.SingleButtonCallback negativeClickListener,
                                  String neutralText , MaterialDialog.SingleButtonCallback neutralClickListener ) {

        MaterialDialog.Builder builder = new MaterialDialog.Builder(context);
        builder.title(title);
        builder.content(message);

        builder.backgroundColor(colorPrimary);
        builder.contentColor(primarytextcolor);

        if ( !positiveText.equals("") || !positiveText.isEmpty() ) {
            builder.positiveText(positiveText);
            builder.onPositive(positiveClickListener);
        }

        if ( !negativeText.equals("") || !negativeText.isEmpty() ) {
            builder.negativeText(negativeText);
            builder.onNegative(negativeClickListener);
        }

        if ( !neutralText.equals("") || !neutralText.isEmpty() ) {
            builder.neutralText(neutralText);
            builder.onNeutral(neutralClickListener);
        }

        builder.show();

    }


    public static String SHARE_FILE_NAME = "game_releases_app_incredible_unique_file_name.png";
    public static final int WRITE_EXTERNAL_STORAGE_PERMISSION = 1;
    public static final int SHAREING_IMAGE = 1337;
    public static File f;
    public static void shareImage(Activity act, Bitmap b) {
        if (ContextCompat.checkSelfPermission(act, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ImageViewerFragment.share_requestPermissions = true;
            ActivityCompat.requestPermissions(act, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_EXTERNAL_STORAGE_PERMISSION);
        } else {

            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("image/jpeg");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            b.compress(Bitmap.CompressFormat.PNG, 100, bytes);

            f = new File(act.getFilesDir(), SHARE_FILE_NAME);

            if ( f.exists() )
                f.delete();

            try {
                f.createNewFile();
                FileOutputStream fo = new FileOutputStream(f);
                fo.write(bytes.toByteArray());
                fo.flush();
                fo.close();
            } catch (IOException exception) {
                exception.printStackTrace();
            }

            debug("MyUtils", "exists: "+f.exists());

            Uri fileUri = FileProvider.getUriForFile(act, "com.nomuffinlabs.gamereleases.handlers.GenericFileProvider", f);

            share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

            share.putExtra(Intent.EXTRA_STREAM, fileUri);
            act.startActivityForResult(Intent.createChooser(share, "Share Image"), SHAREING_IMAGE);
            f.deleteOnExit();
        }
    }

    public static void deleteTemporaryFile() {
        File sdCard = Environment.getExternalStorageDirectory();
        File dir = new File(sdCard.getAbsoluteFile()+"");
        f = new File(dir, SHARE_FILE_NAME);
        if ( f.exists())
            f.delete();
    }

    public static void saveImageInPictures(Activity act, Bitmap b) {
        if (ContextCompat.checkSelfPermission(act, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ImageViewerFragment.save_requestPermissions = true;
            ActivityCompat.requestPermissions(act, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_EXTERNAL_STORAGE_PERMISSION);
        } else {
            MediaStore.Images.Media.insertImage(act.getContentResolver(), b, "title", null);
            MyUtils.mToast("Image saved in /sdcard/Pictures", true, act);
        }
    }

    public static String getAppVersion(Context context) {
        String version = "(Error)";
        try {
            version = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return  version;
    }



    public static void setRecyclerviewStartAnimations(final RecyclerView mRecyclerView, Context context) {

        if ( SharedPrefHelper.getBool(context.getString(R.string.recview_start_animation), true, context) ) {
            mRecyclerView.getViewTreeObserver().addOnPreDrawListener(
                    new ViewTreeObserver.OnPreDrawListener() {

                        @Override
                        public boolean onPreDraw() {

                            mRecyclerView.getViewTreeObserver().removeOnPreDrawListener(this);

                            for (int i = 0; i < mRecyclerView.getChildCount(); i++) {
                                View v = mRecyclerView.getChildAt(i);
                                v.setAlpha(0.0f);
                                v.setTranslationY(300);
                                v.animate().translationY(0)
                                        .alpha(1.0f)
                                        .setDuration(500)
                                        .setStartDelay(i * 100)
                                        .setInterpolator(new DecelerateInterpolator())
                                        .start();
                            }

                            return true;
                        }
                    });
        }
    }



    public static int fetchAccentColor(Context context) {
        TypedValue typedValue = new TypedValue();

        TypedArray a = context.obtainStyledAttributes(typedValue.data, new int[] { R.attr.colorAccent });
        int color = a.getColor(0, 0);

        a.recycle();

        return color;
    }

    public static void showFirstFavoriteTimeReminderDialog(final Context context) { //tell the users about the reminders at first time fav
        if ( !SharedPrefHelper.getBool("first_time_favorite_reminder", false, context)) {

            new MaterialDialog.Builder(context)
                    .title("Reminders for your favorite games")
                    .content("Add a game to your favorites and you will get notifications about the missing days to its release date. " +
                            "The notifications will trigger on 30, 14, 7, 3, 1 and 0 days left. ")
                    //.content("Once you added a game to your favorites you will get notifications about the days left to its release date.")
                    .backgroundColor(MyUtils.colorPrimary)
                    .contentColor(MyUtils.primarytextcolor)
                    .positiveText("Alright")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            SharedPrefHelper.setBool("first_time_favorite_reminder", true, context);
                        }
                    })
                    .show();

        }
    }









    /*public static void testingAlarm(Context context) {

        Log.e("debug", "onreceive");

        Calendar rightNow = Calendar.getInstance();
        int currentHour = rightNow.get(Calendar.HOUR_OF_DAY);

        //if (currentHour != 17)
        //    return;

        ArrayList<Game> favorites = new ArrayList<>();
        FileHandler.loadListFromFile(context, FileHandler.FILE_FAVORITES, favorites, 0);
        Collections.sort(favorites);
        //MainActivity.refreshTabTitles();

        long currentmillis = System.currentTimeMillis() - 86400000; // - 1day


        //going through favorites
        for (int i = 0; i < favorites.size(); i++) {

            ArrayList<ReleaseDate> upcomingGameReleases = new ArrayList<>();

            Game game = favorites.get(i);

            //getting all releasedates from game
            for (int n = 0; n < game.releaseDates.size(); n++) {
                long millis_releasedate = game.releaseDates.get(n).getDate();
                int platform = game.releaseDates.get(n).getPlatform();

                //if later than current time and in current platforms
                if (millis_releasedate > currentmillis && platform_ids.contains(platform)) {


                    //check if already in alarmList
                    boolean inList = false;
                    for (int t = 0; t < upcomingGameReleases.size(); t++) {
                        if (millis_releasedate == upcomingGameReleases.get(t).date2) {
                            inList = true;
                            upcomingGameReleases.get(t).list_platform.add(platform);
                        }
                    }
                    if (!inList)
                        upcomingGameReleases.add(new ReleaseDate(millis_releasedate, new ArrayList<>(Arrays.asList(platform))));


                }
            }

            //going through picked up releasedates that'll release in span
            for (int t = 0; t < upcomingGameReleases.size(); t++) {

                long game_date_millis = upcomingGameReleases.get(t).date2;

                if (game_date_millis > currentmillis) {
                    String text = "";
                    ArrayList<Integer> platforms = upcomingGameReleases.get(t).list_platform;

                    int daysDiff = Math.round(MyUtils.getDaysLeft(game_date_millis, currentmillis));
                    //debug("name: " + game.title + "   daysDiff: " + daysDiff+"          game millis: "+game_date_millis);
                    if (daysDiff == 0) {
                        text = text + "Today on ";
                    } else if (daysDiff == 1) {
                        text = text + "Tomorrow on ";
                    } else if (daysDiff == 3) {
                        text = text + "In Three days on ";
                    } else if (daysDiff == 7) {
                        text = text + "In Seven days on ";
                    } else if (daysDiff == 14) {
                        text = text + "In 14 days on ";
                    } else if (daysDiff == 30) {
                        text = text + "In 30 days on ";
                    }

                    if (!text.isEmpty()) {


                        //TODO to implement a list for notifications
                        //You have to get out the games you need in a list and first THEN create the notifications with the prepared list and the right positions

                        //adding to alarmList of games from notifications. all games should be accessible of all notifications
                        //alarmList.add(game);

                        String string_platform = "";
                        for (int y = 0; y < platforms.size(); y++) {
                            if (platforms.size() == 1)
                                string_platform = string_platform + MyUtils.getPlatformName(platforms.get(y));
                            else if (y == platforms.size() - 1)
                                string_platform = string_platform + "and " + MyUtils.getPlatformName(platforms.get(y));
                            else if (y == platforms.size() - 2)
                                string_platform = string_platform + MyUtils.getPlatformName(platforms.get(y)) + " ";
                            else
                                string_platform = string_platform + MyUtils.getPlatformName(platforms.get(y)) + ", ";
                        }

                        text = text + string_platform;

                        Intent resultIntent = new Intent(context, MainActivity.class);

                        //resultIntent.putExtra(INTENT_EXTRA_NOTIFICATION, true);
                        debug("myutils: id: "+game.id);
                        resultIntent.putExtra("id", game.id);

                        resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                        PendingIntent resultPendingIntent =
                                PendingIntent.getActivity(
                                        context,
                                        (int) System.currentTimeMillis(),
                                        resultIntent,
                                        PendingIntent.FLAG_UPDATE_CURRENT
                                );

                        Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
                        Notification notification = new NotificationCompat.Builder(context, "")
                                .setTicker(game.getTitle())
                                .setSmallIcon(R.drawable.ic_info_white_24dp)
                                .setLargeIcon(bm)
                                .setContentTitle(game.getTitle())
                                .setContentText(text)
                                .setStyle(new NotificationCompat.BigTextStyle()
                                        .bigText(text))
                                .setContentIntent(resultPendingIntent)
                                .setAutoCancel(true)
                                .build();

                        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.notify(game.id, notification);
                    }
                }
            }
        }
    }*/
}
