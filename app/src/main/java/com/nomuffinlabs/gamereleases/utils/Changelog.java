package com.nomuffinlabs.gamereleases.utils;

import android.content.Context;

import com.nomuffinlabs.gamereleases.utils.MyUtils;

public class Changelog {

    public static void showChangelog(Context context) {
        MyUtils.showDialog(context, "Changelog", changelog, "Cool", null, "", null, "", null);
    }

    public static String changelog =
            "1.64:\n" +
            "- Bug Fixes\n" +
            "\n" +
            "1.63:\n" +
            "- Bug Fixes\n" +
            "\n" +
            "1.62:\n" +
            "- Added \"popular games\" section\n" +
            "- Added black and white transition for covers\n" +
            "- Show weeks left option\n" +
            "- Fixed Search & Filter (Shouldn't crash anymore.. hopefully)\n" +
            "- Actually gave the button in the favorites screen a purpose\n" +
            "- Bug fixes\n" +
            "\n" +
            "1.61:\n" +
            "- Bug fixes\n" +
            "\n" +
            "1.60:\n" +
            "- Added option to switch between list and grid view\n" +
            "- Added option to change the accent color\n" +
            "- Added option to hide covers in grid view\n" +
            "- Added Websites\n" +
            "- Added Videos\n" +
            "- Bug fixes\n" +
            "\n" +
            "1.59:\n" +
            "- Added Dark / Light Mode\n" +
            "- Added \"Add to calendar\" feature\n" +
            "- Added \"Hide games with no cover\" feature\n" +
            "- fixed search\n" +
            "- some other bug fixes\n" +
            "\n" +
            "1.58:\n" +
            "- united genres, themes and game modes to categories\n" +
            "- bug fixes\n" +
            "(search might crash currently, not able to fix it at the moment)\n" +
            "\n" +
            "1.57:\n" +
            "- changed package name\n" +
            "- bug fixes\n" +
            "\n" +
            "1.56:\n" +
            "- fixed game release reminders\n" +
            "- bug fixes\n" +
            "\n" +
            "1.55:\n" +
            "- updated background processing of fetching games. Hopefully it works...\n" +
            "- disabled cache feature for now cause of bugs\n" +
            "- fixed bugs\n";

}
