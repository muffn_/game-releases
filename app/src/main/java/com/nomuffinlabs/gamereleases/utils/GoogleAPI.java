package com.nomuffinlabs.gamereleases.utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentSender;
import android.os.Bundle;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.query.SearchableField;
import com.nomuffinlabs.gamereleases.activities.MainActivity;
import com.nomuffinlabs.gamereleases.handlers.FileHandler;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Collections;

import static com.nomuffinlabs.gamereleases.handlers.DataHandler.favoritesList;

/**
 * Created by robin on 23.10.2015.
 */
public class GoogleAPI implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public GoogleAPI(Activity act) {
        this.act = act;
    }

    public GoogleApiClient api;
    private final String GOOGLE_DRIVE_FILE_NAME = "gamereleases_favorites";
    public boolean mResolvingError = false;
    private DriveFile mfile;
    private DriveId driveId;
    private static final int DIALOG_ERROR_CODE = 100;
    public Activity act;

    private static void debug(String message) {
        Log.e("Debug_GoogleAPI", "" + message);
    }

    private void mToast(String message) {
        Log.e("Google_API", "" + message);
        Toast.makeText(act, message, Toast.LENGTH_SHORT).show();
    }

    public void initApi() {
        //sometimes it crashes here? cause of act? maybe null?
        api = new GoogleApiClient.Builder(act)
                .addApi(Drive.API)
                .addScope(Drive.SCOPE_FILE)
                .addScope(Drive.SCOPE_APPFOLDER)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

    }

    public void apiDisconnect() {
        api.disconnect();
    }

    public void apiConnect() {
        api.connect();
    }


    public void doDriveBackup() {
        Drive.DriveApi.newDriveContents(api).setResultCallback(backupContentsCallback);
    }

    public void restoreDriveBackup() {
        final Query query = new Query.Builder()
                .addFilter(Filters.eq(SearchableField.TITLE, GOOGLE_DRIVE_FILE_NAME))
                .build();

        Drive.DriveApi.query(api, query).setResultCallback(new ResultCallback<DriveApi.MetadataBufferResult>() {
            @Override
            public void onResult(DriveApi.MetadataBufferResult metadataBufferResult) {
                /*for(int i = 0 ;i < metadataBufferResult.getMetadataBuffer().getCount() ;i++) {
                    debug("got index "+i);
                    debug("filesize in cloud "+ metadataBufferResult.getMetadataBuffer().get(i).getFileSize());
                    debug("driveId(1): "+ metadataBufferResult.getMetadataBuffer().get(i).getDriveId());
                }*/

                //int count = metadataBufferResult.getMetadataBuffer().getCount() - 1;
                //Date date = metadataBufferResult.getMetadataBuffer().get(0).getCreatedDate();

                if (metadataBufferResult != null) {
                    if (metadataBufferResult.getMetadataBuffer() != null) {
                        if ( metadataBufferResult.getMetadataBuffer().getCount() > 1 ){
                            if (metadataBufferResult.getMetadataBuffer().get(0) != null) {
                                if (metadataBufferResult.getMetadataBuffer().get(0).getDriveId() != null) {
                                    driveId = metadataBufferResult.getMetadataBuffer().get(0).getDriveId();
                                    metadataBufferResult.getMetadataBuffer().release();

                                    //TODO mfile = Drive.DriveApi.getFile(api, driveId);

                                    mfile.open(api, DriveFile.MODE_READ_ONLY, new DriveFile.DownloadProgressListener() {
                                        @Override
                                        public void onProgress(long bytesDown, long bytesExpected) {
                                            //mToast("Downloading... (" + bytesDown + "/" + bytesExpected + ")");;
                                        }
                                    })
                                            .setResultCallback(restoreContentsCallback);
                                    return;
                                }
                            }
                        }
                    }
                }
                //Activity_Settings.dismissDialog();
                //Activity_Main.openSnackbar("No backup found", Snackbar.LENGTH_LONG);
                //return;
            }
        });
    }


    public void parseData(byte[] input) {

        String s = "";
        try {
            s = new String(input, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        FileHandler.writeToFile(act, FileHandler.FILE_FAVORITES, s);

    }

    private ResultCallback<DriveApi.DriveContentsResult> restoreContentsCallback =
            new ResultCallback<DriveApi.DriveContentsResult>() {
                @Override
                public void onResult(DriveApi.DriveContentsResult result) {
                    if (!result.getStatus().isSuccess()) {
                        mToast("Unable to open file, try again.");
//                        act.dismissDialog();
                        return;
                    }

                    DriveContents contents = result.getDriveContents();

                    try {
                        BufferedInputStream in = new BufferedInputStream(contents.getInputStream());

                        //TODO FIX EXPLOIT PROBLEM
                        byte[] buffer = new byte[1024*1024];
                        int n, cnt = 0;

                        //debug("before read " + in.available());

                        while ((n = in.read(buffer)) > 0) {
                            parseData(buffer);
                            debug("n: " + n);
                            debug("buffer: " + buffer[0]);
                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

//                    Activity_Main.gettingGamesForFavBlack();
                    mToast("Restore completed");

                    FileHandler.loadListFromFile(act, FileHandler.FILE_FAVORITES, favoritesList, 0);
                    Collections.sort(favoritesList);
                    MainActivity.favoritesChanged = true;

//                    act.dismissDialog();
                    contents.discard(api);

                }
            };


    private ResultCallback<DriveApi.DriveContentsResult> backupContentsCallback = new ResultCallback<DriveApi.DriveContentsResult>() {

        @Override
        public void onResult(DriveApi.DriveContentsResult result) {
            if (!result.getStatus().isSuccess()) {
                mToast("Error while trying to create new file contents");
                return;
            }

            String mimeType = MimeTypeMap.getSingleton().getExtensionFromMimeType("txt");
            MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
                    .setTitle(GOOGLE_DRIVE_FILE_NAME) // Google Drive File title
                    .setMimeType(mimeType)
                    .setStarred(true).build();
            // create a file on root folder
            Drive.DriveApi.getAppFolder(api)
                    .createFile(api, changeSet, result.getDriveContents())
                    .setResultCallback(backupFileCallback);
        }
    };
    private ResultCallback<DriveFolder.DriveFileResult> backupFileCallback = new ResultCallback<DriveFolder.DriveFileResult>() {
        @Override
        public void onResult(DriveFolder.DriveFileResult result) {
            if (!result.getStatus().isSuccess()) {
                mToast("Error while trying to create the file, try again.");
//                act.dismissDialog();
                return;
            }
            mfile = result.getDriveFile();
            mfile.open(api, DriveFile.MODE_WRITE_ONLY, new DriveFile.DownloadProgressListener() {
                @Override
                public void onProgress(long bytesDownloaded, long bytesExpected) {
                    //TODO act.dialog.setMessage("Creating backup file... (" + bytesDownloaded + "/" + bytesExpected + ")");
                }
            }).setResultCallback(backupContentsOpenedCallback);
        }
    };
    private ResultCallback<DriveApi.DriveContentsResult> backupContentsOpenedCallback = new ResultCallback<DriveApi.DriveContentsResult>() {
        @Override
        public void onResult(DriveApi.DriveContentsResult result) {
            if (!result.getStatus().isSuccess()) {
//                act.dismissDialog();
                mToast("Error opening file, try again.");
                return;
            }

            //act.dialog.setMessage("Backing up..");

            final DriveContents contents = result.getDriveContents();
            BufferedOutputStream bos = new BufferedOutputStream(contents.getOutputStream());

            try {
                // TODO String favorites = DataHandler.getFavoritesAsJson();
                bos.write("".getBytes());
                bos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            contents.commit(api, null).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) {
                    //act.dialog.setMessage("Backup completed!");
                    mToast("Backup completed");
                    //act.dismissDialog();
                }
            });
        }
    };

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        debug("onConnectionFailed");
        if (mResolvingError) { // If already in resolution state, just return.
            //mToast("resolve error == true;");
            debug("1");
            return;
        } else if (result.hasResolution()) { // Error can be resolved by starting an intent with user interaction
            debug("2");
            //act.dismissDialog();
            try {
                //mToast("startResolution");
                result.startResolutionForResult(act, DIALOG_ERROR_CODE);
                mResolvingError = true;
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else { // Error cannot be resolved. Display Error Dialog stating the reason if possible.
            debug("3");
            ErrorDialogFragment fragment = new ErrorDialogFragment();
            Bundle args = new Bundle();
            args.putInt("error", result.getErrorCode());
            fragment.setArguments(args);
            fragment.show(act.getFragmentManager(), "errordialog");
        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        debug("onConnected!");
        //act.dismissDialog();
        //act.mFragment_Settings.showBackupRestoreDialog(act);
    }

    @Override
    public void onConnectionSuspended(int cause) {
        //mToast("Connection suspended");

    }

    public static class ErrorDialogFragment extends DialogFragment {
        public ErrorDialogFragment() {
        }

        public Dialog onCreateDialog(Bundle savedInstanceState) {
            int errorCode = this.getArguments().getInt("error");
            return GooglePlayServicesUtil.getErrorDialog(errorCode, this.getActivity(), DIALOG_ERROR_CODE);
        }

        public void onDismiss(DialogInterface dialog) {
            dialog.dismiss();
        }
    }
}
