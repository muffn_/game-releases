package com.nomuffinlabs.gamereleases.utils;

/**
 * Created by robin on 28.10.2016.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}