package com.nomuffinlabs.gamereleases.utils;

/**
 * Created by robin on 12.10.2016.
 */

public interface HaveInternetCallback {
    void noInternetCallback(int responseCode);
    void haveInternetCallback(int responseCode);
}