package com.nomuffinlabs.gamereleases.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.core.view.GravityCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.nomuffinlabs.gamereleases.UI.ViewPagerAdapter;
import com.nomuffinlabs.gamereleases.handlers.CustomTabActivityHelper;
import com.nomuffinlabs.gamereleases.handlers.DataHandler;
import com.nomuffinlabs.gamereleases.handlers.FavoritesHandler;
import com.nomuffinlabs.gamereleases.models.Game;
import com.nomuffinlabs.gamereleases.utils.MyUtils;
import com.nomuffinlabs.gamereleases.R;
import com.nomuffinlabs.gamereleases.utils.SharedPrefHelper;
import com.nomuffinlabs.gamereleases.fragments.BottomSheetDialogPlatformsFragment;
import com.nomuffinlabs.gamereleases.fragments.ListViewerFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import pub.devrel.easypermissions.EasyPermissions;

import static com.nomuffinlabs.gamereleases.handlers.DataHandler.allPlatforms;
import static com.nomuffinlabs.gamereleases.handlers.DataHandler.releasedList;
import static com.nomuffinlabs.gamereleases.utils.SharedPrefHelper.SHAREDPREF_WANTED_PLATFORMS;
import static com.nomuffinlabs.gamereleases.handlers.DataHandler.favoritesList;
import static com.nomuffinlabs.gamereleases.handlers.DataHandler.filteredMainList;
import static com.nomuffinlabs.gamereleases.handlers.DataHandler.popularList;

public class MainActivity extends AppCompatActivity {


    public Activity act;
    Context context;

    public CustomTabActivityHelper mCustomTabActivityHelper;

    //instances for gameviewpager
/*    public static String INSTANCE_MAIN = "INSTANCE_MAIN";
    public static String INSTANCE_POPULAR = "INSTANCE_POPULAR";
    public static String INSTANCE_FAVORITES = "INSTANCE_FAVORITES";
    public static String INSTANCE_FILTER = "INSTANCE_FILTER";
    public static String INSTANCE_SEARCH = "INSTANCE_SEARCH";
    public static String INSTANCE_NOTIFICATION = INSTANCE_FAVORITES;*/

    public Snackbar snackbar;
    public NavigationView filter_navigationView;
    public DrawerLayout drawer;
    public TabLayout tabLayout;
    public ViewPager viewPager;

    public static final String INSTANCE = "instance";
    public static String LISTVIEWER_MAIN = "All";
    public static String LISTVIEWER_RELEASED = "Released";
    public static String LISTVIEWER_POPULAR = "Popular";
    public static String LISTVIEWER_FAVORITES = "Favorites";

    public static String[] LISTVIEWER_INSTANCES = {LISTVIEWER_RELEASED, LISTVIEWER_POPULAR, LISTVIEWER_MAIN, LISTVIEWER_FAVORITES};
    public ListViewerFragment[] listViewerFragments = new ListViewerFragment[4];

    int colorAccent;

    public static final int NOTIFICATION_INTENT = 5;
    public static final String INTENT_EXTRA_NOTIFICATION = "notification";

    //TODO handle all cases in onRezoom
    public static boolean uiSettingsChanged = false;
    public static boolean qualitySettingsChanged = false;
    public static boolean hideReleasedSettingsChanged = false;
    public static boolean hideNoCoverGamesSettingschanged = false;
    public static boolean favoritesChanged = false; //from google api restore

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        MyUtils.setThemeWithAccent(this);

        super.onCreate(savedInstanceState);

        debug("onCreate");

        act = this;
        context = this;

        setContentView(R.layout.activity_main);

        init();
        onNewIntent(getIntent());

    }

    private void init() {

        if ( SharedPrefHelper.getBool("first_start", true, this) ) {
            //first start things here
            SharedPrefHelper.setBool("first_start", false, this);
        }

        //TODO move to getPreferredPlatforms method
        if ( !SharedPrefHelper.getBool("wantedPlatformsSet", false, this) ) {
            String[] plats = new String[allPlatforms.size()];
            plats = allPlatforms.toArray(plats);
            SharedPrefHelper.setStringSet(SHAREDPREF_WANTED_PLATFORMS, plats, this);
            SharedPrefHelper.setBool("wantedPlatformsSet", true, this);
        }

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Courier.ttf");
        MyUtils.deleteTemporaryFile();

        //UI init
        colorAccent = MyUtils.fetchAccentColor(context);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //viewpager
        viewPager = findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(2);
        viewPager.setOffscreenPageLimit(3);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                //debug("onPageScrolled");
                //refreshMainView();
            }
            @Override public void onPageSelected(int position) {}
            @Override public void onPageScrollStateChanged(int state) {}
        });

        if ( getIntent().getExtras() != null) {
            if (getIntent().getExtras().getBoolean(INTENT_EXTRA_NOTIFICATION)) {
                //viewPager.setCurrentItem(1);
            }
        }

        drawer = findViewById(R.id.drawer_layout);
        filter_navigationView = findViewById(R.id.filter_nav_view);

        // TODO use datahandler platforms to create programmatically
        filter_navigationView.inflateMenu(R.menu.activity_consoles_drawer);
        /*if ( MyUtils.hasDarkTheme(this)) {
            filter_navigationView.inflateMenu(R.menu.activity_consoles_drawer);
        } else {
            filter_navigationView.inflateMenu(R.menu.activity_consoles_drawer_light);
        }*/

        mCustomTabActivityHelper = new CustomTabActivityHelper();

        FavoritesHandler.loadFavorites(this);

        refreshTabTitles();
        initListeners();

        MyUtils.setAlarm(this);

    }



    private void initListeners() {
        filter_navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                int id = item.getItemId();
                debug("id: "+id);
                if (!item.isChecked()) {

                    Intent intent;
                    switch (id) {
                        case R.id.categories:
                            intent = new Intent(act, FilterSelectionActivity.class);
                            break;
                        default:
                            intent = new Intent(act, FilterActivity.class);
                            break;
                    }

                    intent.putExtra(Game.KEY_ID, id);
                    startActivity(intent);
                    if (snackbar != null) {
                        snackbar.dismiss();
                        snackbar = null;
                    }
                }
                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.END);
                return true;
            }
        });

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                //animateFab(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public Bundle getFragmentBundle(String instance) {
        Bundle bundle = new Bundle();
        bundle.putString(INSTANCE, instance);
        return bundle;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        for ( int i = 0; i < LISTVIEWER_INSTANCES.length; i++) {
            listViewerFragments[i] = new ListViewerFragment();

            listViewerFragments[i].setArguments(getFragmentBundle(LISTVIEWER_INSTANCES[i]));

            adapter.addFragment(listViewerFragments[i], LISTVIEWER_INSTANCES[i]);
        }

        viewPager.setAdapter(adapter);
    }


    //TODO for notifications, needs rework still
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

         if ( intent.getExtras() != null ) {
            //check for INTENT_EXTRA_NOTIFICATION didnt work
            Intent newIntent = new Intent(this, GameViewPagerActivity.class);

            int id = intent.getIntExtra("id", -1);

            debug("id: " + id);

            if (id != -1) {

                if ( !notificationOpened.contains(id) ) {

                    int pos = MyUtils.getPosById(favoritesList, id);
                    newIntent.putExtra("pos", pos);
                    newIntent.putExtra("instance", LISTVIEWER_FAVORITES);
                    notificationOpened.add(id);
                    startActivity(newIntent);
                }
            }
        }
    }



    ArrayList<Integer> notificationOpened = new ArrayList<>();

    @Override
    protected void onResume() {
        super.onResume();
        refreshTabTitles();
        refreshFragmentsAdapters();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //FileHandler.writeToFile(this, FileHandler.FILE_FAVORITES, JsonHandler.getFavoritesAsJson()); TODO
    }

    @Override
    protected void onStart() {
        super.onStart();
        mCustomTabActivityHelper.bindCustomTabsService(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mCustomTabActivityHelper.unbindCustomTabsService(this);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if ( MyUtils.hasDarkTheme(this)) {
            inflater.inflate(R.menu.activity_main, menu);
        } else {
            inflater.inflate(R.menu.activity_main_light, menu);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.filter:
                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.END))
                    drawer.closeDrawer(GravityCompat.END);
                else
                    drawer.openDrawer(GravityCompat.END);
                return true;

            case R.id.action_search:
                Intent intent = new Intent(act, SearchActivity.class);
                startActivity(intent);
                return true;

            case R.id.settings:
                //TODO Intent in = new Intent(act, SettingsActivity.class);
                //startActivity(in);
                return true;

            case R.id.filterMainList:
                BottomSheetDialogFragment bottomSheetDialogFragment = new BottomSheetDialogPlatformsFragment(context);
                bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(
                requestCode, permissions, grantResults, this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            DataHandler.orientation_changed = true;
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            DataHandler.orientation_changed = true;
        }
    }



    public void openSnackbar(String text, int time_length) {

        int textColor = getResources().getColor(R.color.primaryTextColor);
        String hexColor = String.format("#%06X", (0xFFFFFF & textColor));

        //TODO does it need to be reinitialized every time?
        snackbar = Snackbar.make(findViewById(R.id.rootlayout), Html.fromHtml("<font color=\""+hexColor+"\">"+text+"</font>"), time_length);
        snackbar.getView().setBackgroundColor(getResources().getColor(R.color.navBarColor));
        snackbar.show();
    }

    public void hideSnackbar() {
        if (snackbar != null)
            if (snackbar.isShown())
                snackbar.dismiss();
    }

    public void refreshMainView() {
        refreshFragmentsAdapters();
        refreshTabTitles();
    }

    public void resetFragments() {
        //reload fragments and all lists
        MyUtils.updateLists(context);
        refreshFragmentsAdapters();
    }

    public void refreshFragmentsAdapters() {
        for (ListViewerFragment fragment : listViewerFragments) {
            if ( fragment != null && fragment.mAdapter != null)
                fragment.mAdapter.notifyDataSetChanged();
        }
    }

    public void refreshTabTitles() {

        if ( tabLayout.getTabAt(0) != null )
            tabLayout.getTabAt(0).setText("Released ("+ releasedList.size() +")");

        if ( tabLayout.getTabAt(2) != null )
            tabLayout.getTabAt(2).setText("All ("+ filteredMainList.size() +")");

        if ( tabLayout.getTabAt(1) != null )
            tabLayout.getTabAt(1).setText("Popular ("+ popularList.size() +")");

        if ( tabLayout.getTabAt(3) != null )
            tabLayout.getTabAt(3).setText("Favorites ("+ favoritesList.size() +")");
    }






    public static void debug(String message) {
        Log.e("Activity_Main: ", message);
    }

}