package com.nomuffinlabs.gamereleases.activities;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.nomuffinlabs.gamereleases.UI.MyAdapter;
import com.nomuffinlabs.gamereleases.utils.OnGamesLoadedCallback;
import com.nomuffinlabs.gamereleases.handlers.DataHandler;
import com.nomuffinlabs.gamereleases.models.Game;
import com.nomuffinlabs.gamereleases.utils.MyUtils;
import com.nomuffinlabs.gamereleases.R;

import java.util.ArrayList;

import static com.nomuffinlabs.gamereleases.activities.MainActivity.debug;

public class SearchActivity extends AppCompatActivity {

    boolean closed = false;

    public static ArrayList<Game> search = new ArrayList<Game>();

    public RecyclerView mRecyclerView;
    MyAdapter mAdapter;
    EditText et_search;
    Toolbar toolbar;
    static Activity act_search;
    String search_query;

    Thread t;
    boolean threadRunning = false;
    boolean newSearchWaiting = false;
    boolean searching = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        act_search = this;
        et_search = findViewById(R.id.search);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final Drawable upArrow = getResources().getDrawable(R.drawable.back_material, null); //TODO TESTEN
        upArrow.setColorFilter(getResources().getColor(R.color.grey), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        MyUtils.setLayoutManager(this, mRecyclerView);

        mAdapter = new MyAdapter(this, mRecyclerView, search);
        mRecyclerView.setAdapter(mAdapter);

        et_search.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ( (event.getAction() == KeyEvent.ACTION_DOWN)
                        && keyCode == KeyEvent.KEYCODE_ENTER ) {

                    if ( searching ) {
                        searching = false;

                        newSearchWaiting = true;

                        newSearch(et_search.getText().toString());
                    } else {
                        loadSearch(et_search.getText().toString());
                    }

                } else if ( (event.getAction() == KeyEvent.ACTION_DOWN) && keyCode == KeyEvent.KEYCODE_BACK ) {
                    onBackPressed();

                }
                return true;
            }
        });
        et_search.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if ( menu != null ) {
                    menu.clear();

                    MenuInflater inflater = getMenuInflater();
                    inflater.inflate(R.menu.activity_search_ok, menu);
                }
            }
            @Override public void afterTextChanged(Editable s) { }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();
        act_search = null;
        closed = true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        act_search = this;
        closed = false;

        if ( et_search.getText().toString().isEmpty() )
            return;

        boolean listChanged = fillSerachList(et_search.getText().toString());
        if ( listChanged ) {
            mAdapter.notifyDataSetChanged();

            if (DataHandler.allGamesFetched && search.size() == 0) {
                MyUtils.showSnackbar(findViewById(R.id.rootlayout), "Sorry, i couldn't find anything", "Sad :(", Snackbar.LENGTH_LONG, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        MyUtils.dismissSnackbar();
                    }
                }).show();

            }
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if (!et_search.getText().toString().equals("")) {
            et_search.setText("");
        } else {
            super.onBackPressed();
        }
    }

    Menu menu;

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        this.menu = menu;

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_search, menu);

        /*if (SharedPrefHelper.getBool("haveList", this)) { // changing to grid
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mRecyclerView.setAdapter(mAdapter);
        } else { //changing to alarmList
            mRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
            mRecyclerView.setAdapter(mAdapter);
        }*/
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.clear:
                if (!et_search.getText().toString().equals("")) {
                    et_search.setText("");
                } else {
                    finish();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void newSearch(final String s) {
        threadRunning = true;
        if (t != null && t.isAlive()) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                search.clear();
                mAdapter.notifyDataSetChanged();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        newSearchWaiting = false;
                        threadRunning = false;
                        loadSearch(s);
                    }
                }, 500);
            }
        }, 500);

    }


    public void loadSearch(final String s) {
        if ( threadRunning )
            return;

        if ( newSearchWaiting )
            return;

        searching = true;

        mRecyclerView.setScrollY(0);

        threadRunning = true;

        debug("loadsearch");

        if (t != null && t.isAlive()) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        t = new Thread(new Runnable() {
            @Override
            public void run() { // background code
                threadRunning = true;
                search_query = s;

                fillSerachList(s);

                if ( act_search == null)
                    return;

                act_search.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.notifyDataSetChanged();

                        if (DataHandler.allGamesFetched && search.size() == 0) {
                            MyUtils.showSnackbar(findViewById(R.id.rootlayout), "Sorry, i couldn't find anything", "Sad :(", Snackbar.LENGTH_LONG, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    MyUtils.dismissSnackbar();
                                }
                            }).show();

                        }
                    }
                });


               /* if (!closed && !DataHandler.allGamesFetched) {
                    if ( search.size() < 30) {
                        DataHandler.loadNextGames(getApplicationContext(), onGamesLoadedCallback);
                    } else {
                        mAdapter.setLoaded(false);
                        if (act_search == null)
                            return;
                        act_search.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                                    @Override
                                    public void onLoadMore() {

                                        //Load data
                                        DataHandler.showLoading(true);
                                        DataHandler.loadNextGames(getApplicationContext(), onGamesLoadedCallback);
                                    }
                                });
                            }
                        });
                    }
                }
                threadRunning = false;*/
            }
        });
        t.start();

    }

    OnGamesLoadedCallback onGamesLoadedCallback = new OnGamesLoadedCallback() {
        public void onStartLoading() {
        }

        @Override
        public void onLoadedCallback(String result) {
            loadSearch(search_query);
        }

        @Override
        public void onError() {

        }
    };


    public boolean fillSerachList(String s) {
        boolean listChanged = false;
       /* for (int i = 0; i < MainActivity.gameList.size(); i++) {
            Game g = MainActivity.gameList.get(i);
            if (g != null) {
                if (g.getTitle().toLowerCase().contains(s)) {
                    if (!MyUtils.listContains(search, g.id)) {
                        search.add(g);
                        listChanged = true;
                    }
                }
            }
        }
        if ( listChanged )
            Collections.sort(search);*/

        return listChanged;
    }


    public static void showLoading(final boolean show) {
        if (act_search != null) {
            act_search.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (show)
                        act_search.findViewById(R.id.loading_bar).setVisibility(View.VISIBLE);
                    else
                        act_search.findViewById(R.id.loading_bar).setVisibility(View.INVISIBLE);

                }
            });
        }
    }

    public static Snackbar snackbar;

    public static void openSnackbar(String text, int time_length) {

        if (act_search == null)
            return;

        int textColor = act_search.getResources().getColor(R.color.primaryTextColor);
        String hexColor = String.format("#%06X", (0xFFFFFF & textColor));

        snackbar = Snackbar.make(act_search.findViewById(R.id.rootlayout), Html.fromHtml("<font color=\"" + hexColor + "\">" + text + "</font>"), time_length);
        snackbar.getView().setBackgroundColor(act_search.getResources().getColor(R.color.navBarColor));
        snackbar.show();
    }

}
