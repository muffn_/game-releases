package com.nomuffinlabs.gamereleases.activities;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.nomuffinlabs.gamereleases.models.Game;
import com.nomuffinlabs.gamereleases.utils.MyUtils;
import com.nomuffinlabs.gamereleases.R;
import com.nomuffinlabs.gamereleases.UI.DepthPageTransformer;
import com.nomuffinlabs.gamereleases.fragments.ImageViewerFragment;

import java.util.ArrayList;

public class ImageViewerPagerActivity extends FragmentActivity {

    private static final int NUM_PAGES = 5;

    ArrayList<String> image_ids;

    ArrayList<String> quality_names;
    ArrayList<String> quality_values;

    ViewPager mPager;
    ScreenSlidePagerAdapter mPagerAdapter;
    Activity act;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyUtils.setThemeWithAccent(this);
        setContentView(R.layout.activity_imageviewerpager);

        act = this;

        Bundle bundle = getIntent().getExtras();

        image_ids = bundle.getStringArrayList("image_ids");
        quality_names = bundle.getStringArrayList("quality_names");
        quality_values = bundle.getStringArrayList("quality_values");

        mPager = findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());

        mPager.setAdapter(mPagerAdapter);
        mPager.setCurrentItem(bundle.getInt("pos"));
        mPager.setPageTransformer(true, new DepthPageTransformer());

    }

    public final int WRITE_EXTERNAL_STORAGE_PERMISSION = 1;
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        debug("onRequestPermissionsResult1");
        switch (requestCode) {
            case WRITE_EXTERNAL_STORAGE_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    ImageViewerFragment frag = (ImageViewerFragment) mPagerAdapter.getRegisteredFragment(mPager.getCurrentItem());

                    Bitmap b = ((BitmapDrawable)frag.image.getDrawable()).getBitmap();
                    if (ImageViewerFragment.share_requestPermissions) {
                        MyUtils.shareImage(act, b);
                        ImageViewerFragment.share_requestPermissions = false;
                    } else if (ImageViewerFragment.save_requestPermissions) {
                        MyUtils.saveImageInPictures(act, b);
                        ImageViewerFragment.save_requestPermissions = false;
                    }
                }
            }
            case MyUtils.SHAREING_IMAGE: {
                if ( MyUtils.f != null)
                    if (MyUtils.f.exists())
                        MyUtils.f.delete();

                break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }




    public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        SparseArray<Fragment> registeredFragments = new SparseArray<>();

        ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            ImageViewerFragment frag = new ImageViewerFragment();
            Bundle bundle = new Bundle();

            bundle.putString(Game.KEY_ID, image_ids.get(position));

            bundle.putStringArrayList("quality_names", quality_names);
            bundle.putStringArrayList("quality_values", quality_values);

            frag.setArguments(bundle);
            return frag;
            //bundle.putInt("image_color");
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        public Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }

        @Override
        public int getCount() {
            return image_ids.size();
        }
    }

    public static void debug(String message) {
        Log.e("Activity_iPager: ", message);
    }

}