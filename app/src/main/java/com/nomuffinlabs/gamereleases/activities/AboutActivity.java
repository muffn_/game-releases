package com.nomuffinlabs.gamereleases.activities;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.nomuffinlabs.gamereleases.utils.MyUtils;
import com.nomuffinlabs.gamereleases.R;

/**
 * Created by robin on 04.01.2016.
 */
public class AboutActivity extends AppCompatActivity {

    boolean isDismissing = false;
    Activity act;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        act = this;

        String version = "(Error)";
        try {
            version = getPackageManager()
                    .getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        TextView about_text = findViewById(R.id.about_text);
        about_text.setText("Game Releases " + version + "\n© nomuffinLabs");

        String hexColor = String.format("#%06X", (0xFFFFFF & MyUtils.primarytextcolor));
        String text = "<font color="+hexColor+">All game data are freely provided by </font> <br/> <font color=#27ae60>IGDB.com</font>";
        ((TextView)findViewById(R.id.provided)).setText(Html.fromHtml(text));
        (findViewById(R.id.provided)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                builder.setToolbarColor(Color.DKGRAY);
                CustomTabsIntent customTabsIntent = builder.build();
                customTabsIntent.launchUrl(act, Uri.parse("https://www.igdb.com"));

                /*if (spackage == null) {
                    Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
                    intent.putExtra(SearchManager.QUERY, "https://www.igdb.com"); // query contains search string
                    Activity_Main.act_favorites.startActivity(intent);
                } else {
                    int color = Color.parseColor("#34373b");
                    CustomTabsIntent customTabsIntent = new CustomTabsIntent.Builder()
                            .setToolbarColor(color)
                            .setShowTitle(true)
                            .build();
                    CustomTabActivityHelper.openCustomTab(
                            Activity_Main.act_favorites,// activity
                            customTabsIntent,
                            Uri.parse("https://www.igdb.com"),
                            null
                    );*/

            }
        });
    }




    public static void debug(String message) {
        Log.e("Activity_Settings: ", message);
    }

    public void dismiss(View view) {
        isDismissing = true;
        setResult(Activity.RESULT_CANCELED);
        if ( MyUtils.hasLollipop())
            finishAfterTransition();
        else
            finish();
    }

    @Override
    public void onBackPressed() {
        dismiss(null);
    }


}