package com.nomuffinlabs.gamereleases.activities;

import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.text.Html;
import android.util.Log;

import com.nomuffinlabs.gamereleases.models.Game;
import com.nomuffinlabs.gamereleases.utils.MyUtils;
import com.nomuffinlabs.gamereleases.R;
import com.nomuffinlabs.gamereleases.fragments.GameViewFragment;

import java.util.ArrayList;
import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by robin on 30.06.2016.
 */
public class GameViewPagerActivity extends FragmentActivity implements EasyPermissions.PermissionCallbacks {

    public ViewPager mPager;
    public GameViewPagerActivity activity;
    public PagerAdapter mPagerAdapter;

    ArrayList<Game> list;
    int current_item = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        MyUtils.setThemeWithAccent(this);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_gameviewpager);

        activity = this;
        current_item = getIntent().getIntExtra("pos", 0);
        list = (ArrayList<Game>) getIntent().getSerializableExtra("list");
        mPager = findViewById(R.id.pager);

        debug("current_item: "+current_item);

        // initiate a ViewPager and a PagerAdapter.
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.setOffscreenPageLimit(1);
        //mPager.setPageTransformer(true, new DepthPageTransformer());
        mPager.setCurrentItem(current_item);
    }

    public void refreshPagerAdapter() {
        mPagerAdapter.notifyDataSetChanged();
        mPager.getAdapter().notifyDataSetChanged();
    }

    //for calendar only
   /* @Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case CalendarHandler.REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != Activity.RESULT_OK) {
                    debug(
                            "This app requires Google Play Services. Please install " +
                                    "Google Play Services on your device and relaunch this app.");
                } else {
                    CalendarHandler.getResultsFromApi();
                }
                break;
            case CalendarHandler.REQUEST_ACCOUNT_PICKER:
                if (resultCode == Activity.RESULT_OK && data != null &&
                        data.getExtras() != null) {
                    String accountName =
                            data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null) {
                        SharedPreferences settings =
                                getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("accountName", accountName);
                        editor.apply();
                        CalendarHandler.mCredential.setSelectedAccountName(accountName);
                        CalendarHandler.getResultsFromApi();
                    }
                }
                break;
            case CalendarHandler.REQUEST_AUTHORIZATION:
                if (resultCode == Activity.RESULT_OK) {
                    CalendarHandler.getResultsFromApi();
                }
                break;

    } }*/


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onPause() {
        super.onPause();

    }



    public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            GameViewFragment frag = new GameViewFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("game", list.get(position));
            frag.setArguments(bundle);

            return frag;
        }

        @Override
        public int getCount() {
            return list.size();
        }

    }


    public Snackbar snackbar;
    public void openSnackbar(String text, int time_length) {
        int textColor = activity.getResources().getColor(R.color.primaryTextColor);
        String hexColor = String.format("#%06X", (0xFFFFFF & textColor));

        snackbar = Snackbar.make(activity.findViewById(R.id.rootlayout), Html.fromHtml("<font color=\""+hexColor+"\">"+text+"</font>"), time_length);
        snackbar.getView().setBackgroundColor(activity.getResources().getColor(R.color.navBarColor));
        snackbar.show();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(
                requestCode, permissions, grantResults, this);
    }


    public static void debug(String message) {
        Log.e("Activity_GamePager: ", message);
    }
}
