package com.nomuffinlabs.gamereleases.activities;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.nomuffinlabs.gamereleases.R;

/**
 * Created by robin on 29.10.2016.
 */

public class FilterSelectionActivity extends AppCompatActivity {

    public ListView listview;
    ArrayAdapter<String> itemsAdapter;
    
    Toolbar toolbar;
    int id = R.id.categories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_selection);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        listview = findViewById(R.id.listview);


    }

    @Override
    protected void onResume() {
        super.onResume();

        getSupportActionBar().setTitle("Sort by categories");
        showSelection();

    }

    public void showSelection() {


        /*Collections.sort(MainActivity.game_modes);
        Collections.sort(MainActivity.genres);
        Collections.sort(MainActivity.themes);

        ArrayList<String> cats = new ArrayList<>();
        cats.addAll(MainActivity.game_modes);
        cats.addAll(MainActivity.genres);
        cats.addAll(MainActivity.themes);

        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, cats);
        listview.setAdapter(itemsAdapter);
        listview.setVisibility(View.VISIBLE);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long idd) {
                final String item = (String) parent.getItemAtPosition(position);

                Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                intent.putExtra(KEY_ID, id);
                intent.putExtra(FilterActivity.KEY_FILTER_VALUE, item);
                startActivity(intent);
            }
        });*/
    }


   /* public void showGameModesSelection() {
    	List<String> list = new ArrayList<>(MainActivity.game_modes);

        Collections.sort(MainActivity.game_modes);

        itemsAdapter =
                new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);
		listview.setAdapter(itemsAdapter);
		listview.setVisibility(View.VISIBLE);
		
		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long idd) {
                final String item = (String) parent.getItemAtPosition(position);

                Intent intent = new Intent(getApplicationContext(),
                        FilterActivity.class);
                intent.putExtra(KEY_ID, id);
                intent.putExtra("filter_value", position + 1);
                startActivity(intent);
            }
        });
    }

    public void showThemesSelection() {

        Collections.sort(MainActivity.themes);
    	
    	ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, MainActivity.themes);
		listview.setAdapter(itemsAdapter);
		listview.setVisibility(View.VISIBLE);
		
		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        	@Override
        	public void onItemClick(AdapterView<?> parent, final View view, int position, long idd) {
                final String item = (String) parent.getItemAtPosition(position);

                Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                intent.putExtra(KEY_ID, id);
                intent.putExtra(FilterActivity.KEY_FILTER_VALUE, item);
                startActivity(intent);
            }
    	});

    
    }
    public void showGenresSelection() {
    
        Collections.sort(MainActivity.genres);

        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, MainActivity.genres);
		listview.setAdapter(itemsAdapter);
		listview.setVisibility(View.VISIBLE);
		
		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        	@Override
        	public void onItemClick(AdapterView<?> parent, final View view, int position, long idd) {
                final String item = (String) parent.getItemAtPosition(position);

                Intent intent = new Intent(getApplicationContext(), FilterActivity.class);
                intent.putExtra(KEY_ID, id);
                intent.putExtra(FilterActivity.KEY_FILTER_VALUE, item);
                startActivity(intent);
            }
    	});
    
    }*/
    


}
