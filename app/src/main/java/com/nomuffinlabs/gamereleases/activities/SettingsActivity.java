package com.nomuffinlabs.gamereleases.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceScreen;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.nomuffinlabs.gamereleases.utils.GoogleAPI;
import com.nomuffinlabs.gamereleases.utils.MyUtils;
import com.nomuffinlabs.gamereleases.R;

import static com.nomuffinlabs.gamereleases.utils.MyUtils.mToast;

/*
public class SettingsActivity extends AppCompatActivity implements
        PreferenceFragmentCompat.OnPreferenceStartScreenCallback,
        PreferenceScreenNavigationStrategy.ReplaceFragment.Callbacks {

    Toolbar mToolbar;

    public SettingsFragment mFragment_Settings;
    private PreferenceScreenNavigationStrategy.ReplaceFragment mReplaceFragmentStrategy;
    public GoogleAPI api;


    public Activity act;
    public ProgressDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        act = this;
        api = new GoogleAPI(this);
        api.initApi();

        MyUtils.setThemeWithAccent(this);
        setContentView(R.layout.activity_settings);
        mReplaceFragmentStrategy = new PreferenceScreenNavigationStrategy.ReplaceFragment(this, R.anim.abc_fade_in, R.anim.abc_fade_out, R.anim.abc_fade_in, R.anim.abc_fade_out);

        if (savedInstanceState == null) {
            mFragment_Settings = SettingsFragment.newInstance(null);
            getSupportFragmentManager().beginTransaction().add(R.id.content, mFragment_Settings, "Settings").commit();
        } else {
            mFragment_Settings = (SettingsFragment) getSupportFragmentManager().findFragmentByTag("Settings");
        }

        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        mToolbar.setTitle("Settings");
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });



        //MyUtils.testingAlarm(this);

    }

    @Override
    public boolean onPreferenceStartScreen(final PreferenceFragmentCompat preferenceFragmentCompat, final PreferenceScreen preferenceScreen) {
        mReplaceFragmentStrategy.onPreferenceStartScreen(getSupportFragmentManager(), preferenceFragmentCompat, preferenceScreen);
        return true;
    }

    @Override
    public PreferenceFragmentCompat onBuildPreferenceFragment(final String rootKey) {
        return SettingsFragment.newInstance(rootKey);
    }








    private static final int DIALOG_ERROR_CODE = 100;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        debug("onActivityResult: "+requestCode);
        if (requestCode == DIALOG_ERROR_CODE) {
            debug("DIALOG_ERROR_CODE: "+resultCode);
            api.mResolvingError = false;
            if (resultCode == RESULT_OK) { // Error was resolved, now connect to the client if not done so.
                debug("RESULT_OK");
                if (!api.api.isConnecting() && !api.api.isConnected()) {
                    debug("apiConnect");
                    api.apiConnect();
                } else {
                    mToast("Something went wrong (1)", true, this);
                }
            } else {
                mToast("Something went wrong (2)", true, this);
            }
        } else {
            mToast("Something went wrong (3)", true, this);
        }
    }

    public void dismissDialog() {
        if (dialog != null)
            dialog.dismiss();
    }

    public void showProgress() {
        if ( act != null)
            dialog = new ProgressDialog(act);

        if ( dialog != null ) {
            dialog.setTitle("Loading...");
            dialog.setMessage("Please wait");
            dialog.setCancelable(false);
            dialog.show();
        }
    }




    public void debug(String message) {
        Log.e("Activity_Settings: ", message);
    }


}*/