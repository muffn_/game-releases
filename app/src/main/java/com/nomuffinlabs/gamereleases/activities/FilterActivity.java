package com.nomuffinlabs.gamereleases.activities;

import android.app.Activity;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.view.View;

import com.nomuffinlabs.gamereleases.UI.MyAdapter;
import com.nomuffinlabs.gamereleases.models.Game;
import com.nomuffinlabs.gamereleases.R;

import java.util.ArrayList;

/**
 * Created by robin on 29.10.2016.
 */

public class FilterActivity extends AppCompatActivity {

    public static ArrayList<Game> gameList_filtered = new ArrayList<>();

    public MyAdapter mAdapter_filter;
    public Activity activity;

    public RecyclerView mRecyclerView;

    public static String KEY_FILTER_VALUE = "filter_value";

    boolean closed = false;
    Toolbar toolbar;
    public int filter = -1; //-1 = everything/cats; 0 = gamemodes; 1 = genres; 2 = themes; 3 = consoles;
    public String filter_value = "";

    Thread t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        /*act_filter = this;
        Bundle bundle = getIntent().getExtras();
        int id = bundle.getInt(Game.KEY_ID);

        DataHandler.showLoading(false);

        mRecyclerView_filter = findViewById(R.id.recyclerView);
        mRecyclerView_filter.setHasFixedSize(true);

        mAdapter_filter = new MyAdapter(this, mRecyclerView_filter, gameList_filtered, MainActivity.INSTANCE_FILTER);

        MyUtils.setLayoutManager(this, mRecyclerView_filter);
        mRecyclerView_filter.setAdapter(mAdapter_filter);

        MyUtils.setRecyclerviewStartAnimations(mRecyclerView_filter, this);

        switch (id) {
            case R.id.categories:
                filter = -1;
                filter_value = bundle.getString(KEY_FILTER_VALUE);
                getSupportActionBar().setTitle(filter_value);
                break;
            case R.id.gamemodes:
                filter = 0;
                filter_value = bundle.getString(KEY_FILTER_VALUE);
                getSupportActionBar().setTitle(filter_value);
                break;
            case R.id.genres:
                filter = 1;
                filter_value = bundle.getString(KEY_FILTER_VALUE);
                getSupportActionBar().setTitle(filter_value);
                break;
            case R.id.themes:
                filter = 2;
                filter_value = bundle.getString(KEY_FILTER_VALUE);
                getSupportActionBar().setTitle(filter_value);
                break;
            case R.id.win:
                getSupportActionBar().setTitle("Windows");
                mRecyclerView_filter.setVisibility(View.VISIBLE);
                filter = 3;
                filter_value = MainActivity.platform_names.get(0);
                break;
            case R.id.xone:
                getSupportActionBar().setTitle("Xbox One");
                mRecyclerView_filter.setVisibility(View.VISIBLE);
                filter = 3;
                filter_value = MainActivity.platform_names.get(1);
                break;
            case R.id.ps4:
                getSupportActionBar().setTitle("Playstation 4");
                mRecyclerView_filter.setVisibility(View.VISIBLE);
                filter = 3;
                filter_value = MainActivity.platform_names.get(2);
                break;
            case R.id.nswitch:
                getSupportActionBar().setTitle("Nintendo Switch");
                mRecyclerView_filter.setVisibility(View.VISIBLE);
                filter = 3;
                filter_value = MainActivity.platform_names.get(3);
                break;
            case R.id.nds:
                getSupportActionBar().setTitle("Nintendo 3ds");
                mRecyclerView_filter.setVisibility(View.VISIBLE);
                filter = 3;
                filter_value = MainActivity.platform_names.get(4);
                break;
        }*/

        gameList_filtered.clear();

        loadFilteredGames();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onStop() {
        super.onStop();

        closed = true;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        MainActivity.debug("onDestroy");

        if (t != null && t.isAlive()) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        closed = false;
        mAdapter_filter.notifyDataSetChanged();
    }

    public void showLoading(final boolean show) {
        if (show)
            findViewById(R.id.loading_bar).setVisibility(View.VISIBLE);
        else
            findViewById(R.id.loading_bar).setVisibility(View.INVISIBLE);
    }

    public void loadFilteredGames() {
/*
        //thread already alive? wait for it to finish/close
        if (t != null && t.isAlive()) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        t = new Thread(new Runnable() {
            @Override
            public void run() { // background code

                addMissingGamesToFilteredList();

                if ( act_filter == null)
                    return;

                act_filter.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter_filter.notifyDataSetChanged();
                        GameViewPagerActivity.refreshPagerAdapter();

                        if (DataHandler.allGamesFetched && gameList_filtered.size() == 0) {
                            MyUtils.showSnackbar(findViewById(R.id.rootlayout), "Sorry, i couldn't find anything", "Sad :(", Snackbar.LENGTH_LONG, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    MyUtils.dismissSnackbar();
                                }
                            }).show();

                        }
                    }
                });

                if (!closed && !DataHandler.allGamesFetched) {

                    debug("gamelist_filtered: " + gameList_filtered.size());

                    if (gameList_filtered.size() < 30) {
                        DataHandler.loadNextGames(getApplicationContext(), new OnGamesLoadedCallback() {
                            public void onStartLoading() {
                            }

                            @Override
                            public void onLoadedCallback(String result) {
                                loadFilteredGames();
                            }

                            @Override
                            public void onError() {
                            }
                        });
                    } else {
                        mAdapter_filter.setLoaded(false);
                        if (act_filter == null)
                            return;

                        act_filter.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                mAdapter_filter.setOnLoadMoreListener(new OnLoadMoreListener() {
                                    @Override
                                    public void onLoadMore() {
                                        //Load data
                                        DataHandler.showLoading(true);
                                        DataHandler.loadNextGames(getApplicationContext(), new OnGamesLoadedCallback() {
                                            public void onStartLoading() {
                                            }

                                            @Override
                                            public void onLoadedCallback(String result) {


                                                loadFilteredGames();

                                            }

                                            @Override
                                            public void onError() {

                                            }
                                        });
                                    }
                                });

                            }
                        });

                    }
                }
            }
        });
        t.start();*/

    }


    public void addMissingGamesToFilteredList() {

/*
        for (int i = 0; i < gameList.size(); i++) {

            Game g = gameList.get(i);

            switch (filter) { //filter > 0 = gamemodes; 1 = genres; 2 = themes; 3 = consoles; -1 = everything/categories/cats
                case -1:
                    if (g.categories.contains(filter_value)) {
                        if (!MyUtils.listContains(gameList_filtered, g.getId()))
                            gameList_filtered.add(g);
                    }
                    break;
                case 0:
                    //by game modes
                    if (g.game_modes.contains(filter_value)) {
                        if (!MyUtils.listContains(gameList_filtered, g.getId()))
                            gameList_filtered.add(g);
                    }
                    break;
                case 1:
                    //by genres
                    if (g.genres.contains(filter_value)) {
                        if (!MyUtils.listContains(gameList_filtered, g.getId()))
                            gameList_filtered.add(g);
                    }
                    break;
                case 2:
                    //by themes
                    if (g.themes.contains(filter_value)) {
                        if (!MyUtils.listContains(gameList_filtered, g.getId()))
                            gameList_filtered.add(g);
                    }
                    break;
                case 3:
                    //by platform

                    //already in list?
                    if (!MyUtils.listContains(gameList_filtered, g.id)) {

                        Game newGame = SerializationUtils.clone(g);
                        for (int n = 0; n < newGame.releaseDates.size(); n++) {
                            ReleaseDate current_releasedate = newGame.releaseDates.get(n);
                            int current_platform = current_releasedate.platform;

                            //we only have the name so get it by indexOf() from the ids list
                            int idOfFilteredValue = MainActivity.platform_ids.get(MainActivity.platform_names.indexOf(filter_value));
                            if (idOfFilteredValue != current_platform) {
                                newGame.releaseDates.remove(n);
                                n = -1;
                            }
                        }
                        if (newGame.releaseDates.size() > 0) {
                            gameList_filtered.add(newGame);
                        }
                    }
                    break;
            }
        }

        Collections.sort(gameList_filtered);*/

    }


    public static Snackbar snackbar;

    public void openSnackbar(String text, int time_length) {
        int textColor = getResources().getColor(R.color.primaryTextColor);
        String hexColor = String.format("#%06X", (0xFFFFFF & textColor));

        snackbar = Snackbar.make(findViewById(R.id.rootlayout), Html.fromHtml("<font color=\"" + hexColor + "\">" + text + "</font>"), time_length);
        snackbar.getView().setBackgroundColor(getResources().getColor(R.color.navBarColor));
        snackbar.show();
    }
}
